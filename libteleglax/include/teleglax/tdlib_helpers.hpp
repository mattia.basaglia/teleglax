#ifndef TELEGLAX_TDLIB_HELPERS_HPP
#define TELEGLAX_TDLIB_HELPERS_HPP

#include <type_traits>
#include <td/telegram/td_api.hpp>


namespace teleglax {

namespace detail {

template <class TargetType>
struct TdCompatible
{
    mutable bool result = false;

    void operator()(td::td_api::Object&) const
    {}

    void operator()(TargetType&) const
    {
        result = true;
    }
};

template<>
struct TdCompatible<td::td_api::Object>
{
    mutable bool result = false;

    void operator()(td::td_api::Object&) const
    {
        result = true;
    }
};


} // namespace detail


template <class TargetType>
inline bool td_is_compatible(td::td_api::object_ptr<td::td_api::Object>& object)
{
    detail::TdCompatible<TargetType> cb;
    td::td_api::downcast_call(*object, cb);
    return cb.result;
}

} // namespace teleglax

#endif // TELEGLAX_TDLIB_HELPERS_HPP
