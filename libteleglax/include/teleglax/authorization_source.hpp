#ifndef TELEGLAX_AUTHORIZATION_SOURCE_HPP
#define TELEGLAX_AUTHORIZATION_SOURCE_HPP
#include <string>
#include <memory>

#include <td/telegram/td_api.h>

namespace teleglax {

class AuthorizationInput
{
public:
    struct Code
    {
        std::string code;
        std::string first_name;
        std::string last_name;
    };

    struct PhoneNumber
    {
        std::string phone_number;
        bool allow_flash_call = false;
        bool is_current_phone_number = false;
    };

    virtual ~AuthorizationInput(){}

    virtual Code get_code(td::td_api::authorizationStateWaitCode state) = 0;
    virtual std::string get_password(td::td_api::authorizationStateWaitPassword state) = 0;
    virtual PhoneNumber get_phone_number() = 0;
    virtual bool is_bot() = 0;
    virtual std::string get_bot_token() = 0;
    virtual void on_auth_complete(bool logged_in) {}
    virtual void on_auth_logging_out() {}
};

class BotAuthorization : public AuthorizationInput
{
public:
    explicit BotAuthorization(std::string token)
        : token(std::move(token))
    {}

    Code get_code(td::td_api::authorizationStateWaitCode state) override
    {
        return {};
    }

    std::string get_password(td::td_api::authorizationStateWaitPassword state) override
    {
        return {};
    }

    PhoneNumber get_phone_number() override
    {
        return {};
    }

    bool is_bot() override
    {
        return true;
    }

    std::string get_bot_token() override
    {
        return token;
    }

    void set_bot_token(const std::string& token)
    {
        this->token = token;
    }

private:
    std::string token;
};

class AuthSource
{
public:
    template<class T, class... Args>
        AuthSource(Args&&... args)
         : data(std::make_unique<T>(std::forward<Args>(args)...))
         {}

    template<class T>
        AuthSource(T&& arg)
         : data(std::make_unique<T>(std::forward<T>(arg)))
         {}

    AuthorizationInput::Code get_code(td::td_api::authorizationStateWaitCode state) const
    {
        return data->get_code(std::move(state));
    }

    std::string get_password(td::td_api::authorizationStateWaitPassword state) const
    {
        return data->get_password(std::move(state));
    }

    AuthorizationInput::PhoneNumber get_phone_number() const
    {
        return data->get_phone_number();
    }

    bool is_bot() const
    {
        return data->is_bot();
    }

    std::string get_bot_token() const
    {
        return data->get_bot_token();
    }

    void on_auth_complete(bool logged_in) const
    {
        return data->on_auth_complete(logged_in);
    }

    AuthorizationInput* source() const
    {
        return data.get();
    }

    void on_auth_logging_out() const
    {
        return data->on_auth_logging_out();
    }

private:
    std::unique_ptr<AuthorizationInput> data;
};

} // namespace teleglax
#endif // TELEGLAX_AUTHORIZATION_SOURCE_HPP
