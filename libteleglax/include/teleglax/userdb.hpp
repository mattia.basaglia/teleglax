#ifndef TELEGLAX_USERDB_HPP
#define TELEGLAX_USERDB_HPP

#include <unordered_map>
#include <memory>

#include <td/telegram/td_api.h>

namespace teleglax {

template<class WrappedType, class IdType, class ValueT=std::shared_ptr<WrappedType>>
class DbWrapper
{
public:
    using key_type = IdType;
    using value_type = ValueT;
    using mapping_type = std::unordered_map<key_type, value_type>;
    using iterator = typename mapping_type::const_iterator;

    value_type& insert(key_type id, value_type obj)
    {
        return db.emplace(id, std::move(obj)).first->second;
    }

    void erase(key_type id)
    {
        auto it = db.find(id);
        if ( it != db.end() )
            db.erase(it);
    }

    value_type operator[](key_type id) const
    {
        return get(id);
    }

    value_type get(key_type id) const
    {
        auto it = db.find(id);
        if ( it != db.end() )
            return it->second;
        return {};
    }

    iterator begin() const
    {
        return db.begin();
    }
    iterator end() const
    {
        return db.end();
    }
    iterator find(key_type id) const
    {
        return db.find(id);
    }

    auto size() const
    {
        return db.size();
    }

protected:
    typename mapping_type::iterator begin()
    {
        return db.begin();
    }
    typename mapping_type::iterator end()
    {
        return db.end();
    }
    typename mapping_type::iterator find(key_type id)
    {
        return db.find(id);
    }

    mapping_type db;
};


class UserDb : public DbWrapper<td::td_api::user, std::int32_t>
{
public:
    void update_status(key_type id, td::td_api::object_ptr<td::td_api::UserStatus> status)
    {
        if ( auto it = find(id); it != end() )
            it->second->status_ = std::move(status);
    }

    using DbWrapper::insert;
    auto& insert(td::td_api::object_ptr<td::td_api::user> obj)
    {
        key_type id  = obj->id_;
        return insert(id, std::move(obj));
    }

};

struct ChatData
{
    std::shared_ptr<td::td_api::chat> chat;
    std::shared_ptr<td::td_api::Object> details;
    std::shared_ptr<td::td_api::Object> full_info;


    std::int32_t td_related_id() const
    {
        return td_related_id(chat);
    }

    static std::int32_t td_related_id(const std::shared_ptr<td::td_api::chat>& chat)
    {
        switch ( chat->type_->get_id() )
        {
            case td::td_api::chatTypePrivate::ID:
                return static_cast<td::td_api::chatTypePrivate*>(chat->type_.get())->user_id_;
            case td::td_api::chatTypeBasicGroup::ID:
                return static_cast<td::td_api::chatTypeBasicGroup*>(chat->type_.get())->basic_group_id_;
            case td::td_api::chatTypeSupergroup::ID:
                return static_cast<td::td_api::chatTypeSupergroup*>(chat->type_.get())->supergroup_id_;
            case td::td_api::chatTypeSecret::ID:
                return static_cast<td::td_api::chatTypeSecret*>(chat->type_.get())->user_id_;
            default:
                return 0;

        }
    }
};

class ChatDb : public DbWrapper<td::td_api::chat, std::int64_t, ChatData>
{
public:
    void update_order(key_type id, std::int64_t order)
    {
        if ( auto it = find(id); it != end() )
            it->second.chat->order_ = order;
    }

    void update_last_read_outbox(key_type id, std::int64_t message_id)
    {
        if ( auto it = find(id); it != end() )
            it->second.chat->last_read_outbox_message_id_ = message_id;
    }

    void update_last_read_inbox(key_type id, std::int64_t message_id)
    {
        if ( auto it = find(id); it != end() )
            it->second.chat->last_read_inbox_message_id_ = message_id;
    }

    void update_title(key_type id, const std::string& title)
    {
        if ( auto it = find(id); it != end() )
            it->second.chat->title_ = title;
    }

    std::int64_t update_details(key_type id, std::shared_ptr<td::td_api::Object> obj)
    {
        for ( auto it = begin(); it != end(); ++it )
            if ( it->second.td_related_id() == id )
            {
                it->second.details = std::move(obj);
                return it->second.chat->id_;
            }
        return 0;
    }

    std::int64_t update_full_info(key_type id, td::td_api::object_ptr<td::td_api::Object> obj)
    {
        if ( auto it = find(id); it != end() )
            if ( it->second.td_related_id() == id )
            {
                it->second.full_info = std::move(obj);
                return it->second.chat->id_;
            }
        return 0;
    }

    void update_last_message(key_type id, td::td_api::object_ptr<td::td_api::message> msg)
    {
        if ( auto it = find(id); it != end() )
            it->second.chat->last_message_ = std::move(msg);
    }

    void update_photo(key_type id, td::td_api::object_ptr<td::td_api::chatPhoto> photo)
    {
        if ( auto it = find(id); it != end() )
            it->second.chat->photo_ = std::move(photo);
    }

    void update_is_pinned(key_type id, bool pinned)
    {
        if ( auto it = find(id); it != end() )
            it->second.chat->is_pinned_ = pinned;
    }

    void update_reply_markup(key_type id, std::int64_t reply_id)
    {
        if ( auto it = find(id); it != end() )
            it->second.chat->reply_markup_message_id_ = reply_id;
    }

    using DbWrapper::insert;
    ChatData& insert(td::td_api::object_ptr<td::td_api::chat> obj)
    {
        key_type id  = obj->id_;
        auto it = find(id);
        if ( it == db.end() )
            return db.emplace(id, ChatData{std::move(obj), {}, {}}).first->second;

        it->second.chat = std::move(obj);
        return it->second;
    }

};

} // namespace teleglax
#endif // TELEGLAX_USERDB_HPP
