#ifndef TELEGLAX_CALLBACK_HPP
#define TELEGLAX_CALLBACK_HPP

#include <type_traits>
#include <memory>

#include "exceptions.hpp"
#include "tdlib_helpers.hpp"


namespace teleglax {
namespace detail {

template<class T>
    struct decay_member_function
    {
        using type = T;
    };

template<class Ret, class Owner, class... Args>
    struct decay_member_function<Ret (Owner::*) (Args...)>
    {
        using type = Ret(*)(Args...);
    };

template<class Ret, class Owner, class... Args>
    struct decay_member_function<Ret (Owner::*) (Args...) const>
    {
        using type = Ret(*)(Args...);
    };

template<class Ret, class Owner, class... Args>
    struct decay_member_function<Ret (Owner::*) (Args...) &&>
    {
        using type = Ret(*)(Args...);
    };

template<class T>
struct is_functor
{
private:
    template <class Callable>
        static decltype(&Callable::operator()) _test(int);
    template <class Callable>
        static T _test(...);

public:
    using type = decltype(_test<std::decay_t<T>>(1));
    static constexpr bool value = !std::is_same<T, type>::value;
};

template<class T>
struct is_funcptr_ptr : std::false_type
{
    using contained_type = void;
};

template<class Ret, class Arg>
    struct is_funcptr_ptr<Ret(*)(const std::unique_ptr<Arg> &)> : std::true_type
{
    using contained_type = Arg;
};

template<class Ret, class Arg>
    struct is_funcptr_ptr<Ret(*)(std::unique_ptr<Arg> &)> : std::true_type
{
    using contained_type = Arg;
};

template<class Ret, class Arg>
    struct is_funcptr_ptr<Ret(*)(std::unique_ptr<Arg> &&)> : std::true_type
{
    using contained_type = Arg;
};

template<class Ret, class Arg>
    struct is_funcptr_ptr<Ret(*)(std::unique_ptr<Arg>)> : std::true_type
{
    using contained_type = Arg;
};

template<class T>
struct is_funcptr_callable : public is_funcptr_ptr<typename decay_member_function<typename is_functor<T>::type>::type> {};


template<class T>
struct is_funcref_ptr : std::false_type
{
    using contained_type = void;
};

template<class Ret, class Arg>
    struct is_funcref_ptr<Ret(*)(const Arg &)> : std::true_type
{
    using contained_type = Arg;
};

template<class Ret, class Arg>
    struct is_funcref_ptr<Ret(*)(Arg &)> : std::true_type
{
    using contained_type = Arg;
};

template<class Ret, class Arg>
    struct is_funcref_ptr<Ret(*)(Arg &&)> : std::true_type
{
    using contained_type = Arg;
};

template<class Ret, class Arg>
    struct is_funcref_ptr<Ret(*)(Arg)> : std::true_type
{
    using contained_type = Arg;
};

template<class T>
struct is_funcref_callable : public is_funcref_ptr<typename decay_member_function<typename is_functor<T>::type>::type> {};

} // namespace detail


class TeleglaxCallback
{
private:
    class Holder
    {
    public:
        virtual ~Holder(){}
        virtual void invoke(td::td_api::object_ptr<td::td_api::Object> object) = 0;
    };

    template<class Callback, template<class> class CallableTraits>
    class HolderCall : public Holder
    {
    public:
        using object_type = typename CallableTraits<Callback>::contained_type;

        HolderCall(Callback callback) : callback(std::move(callback)) {}

        void check_compatible(td::td_api::object_ptr<td::td_api::Object>& object) const
        {
            if ( !td_is_compatible<object_type>(object) )
            {
                throw UnknownObjectError(*object, "callback argument");
            }
        }

        td::td_api::object_ptr<object_type> moved(td::td_api::object_ptr<td::td_api::Object>& object) const
        {
            return td::move_tl_object_as<object_type>(object);
        }

        Callback callback;
    };

    template<class Callback>
    class HolderPtrCall : public HolderCall<Callback, detail::is_funcptr_callable>
    {
    public:
        using HolderCall<Callback, detail::is_funcptr_callable>::HolderCall;

        void invoke(td::td_api::object_ptr<td::td_api::Object> object) override
        {
            this->check_compatible(object);
            this->callback(this->moved(object));
        }
    };

    template<class Callback>
    class HolderRefCall : public HolderCall<Callback, detail::is_funcref_callable>
    {
    public:
        using HolderCall<Callback, detail::is_funcref_callable>::HolderCall;

        void invoke(td::td_api::object_ptr<td::td_api::Object> object) override
        {
            this->check_compatible(object);
            this->callback(*this->moved(object));
        }
    };

public:
    TeleglaxCallback() = default;

    template<class T, class = std::enable_if_t<detail::is_funcptr_callable<T>::value> >
        TeleglaxCallback(T func)
        : holder(std::make_unique<HolderPtrCall<std::decay_t<T>>>(std::forward<T>(func)))
        {}

    template<class T, class = std::enable_if_t<!detail::is_funcptr_callable<T>::value && detail::is_funcref_callable<T>::value>, class = void>
        TeleglaxCallback(T func)
        : holder(std::make_unique<HolderRefCall<std::decay_t<T>>>(std::forward<T>(func)))
        {}


    void operator()(td::td_api::object_ptr<td::td_api::Object> object) const
    {
        if ( holder )
            holder->invoke(std::move(object));
    }

    explicit operator bool() const
    {
        return bool(holder);
    }

private:
    std::unique_ptr<Holder> holder;
};

} // namespace teleglax
#endif // TELEGLAX_CALLBACK_HPP
