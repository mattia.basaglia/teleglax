
namespace teleglax {

template<>
void Teleglax::on_auth_state(td::td_api::authorizationStateReady&)
{
    auth_status = AuthStatus::Authed;
    auth.on_auth_complete(true);
}

template<>
void Teleglax::on_auth_state(td::td_api::authorizationStateLoggingOut&)
{
    auth_status = AuthStatus::Unauthed;
    auth.on_auth_logging_out();
}

template<>
void Teleglax::on_auth_state(td::td_api::authorizationStateClosing&)
{
}

template<>
void Teleglax::on_auth_state(td::td_api::authorizationStateClosed&)
{
    auth_status = AuthStatus::Unauthed;
    auth.on_auth_complete(false);
}

template<>
void Teleglax::on_auth_state(td::td_api::authorizationStateWaitCode& state)
{
    AuthorizationInput::Code code = auth.get_code(std::move(state));
    internal_request<td::td_api::checkAuthenticationCode>(code.code, code.first_name, code.last_name);
}

template<>
void Teleglax::on_auth_state(td::td_api::authorizationStateWaitPassword& state)
{
    internal_request<td::td_api::checkAuthenticationPassword>(auth.get_password(std::move(state)));
}

template<>
void Teleglax::on_auth_state(td::td_api::authorizationStateWaitPhoneNumber&)
{
    if ( auth.is_bot() )
    {
        internal_request<td::td_api::checkAuthenticationBotToken>(
             auth.get_bot_token()
        );
    }
    else
    {
        auto pn = auth.get_phone_number();
        internal_request<td::td_api::setAuthenticationPhoneNumber>(
            pn.phone_number,
            pn.allow_flash_call,
            pn.is_current_phone_number
        );
    }
}

template<>
void Teleglax::on_auth_state(td::td_api::authorizationStateWaitEncryptionKey&)
{
    internal_request<td::td_api::checkDatabaseEncryptionKey>("");
}

template<>
void Teleglax::on_auth_state(td::td_api::authorizationStateWaitTdlibParameters&)
{
    internal_request<td::td_api::setTdlibParameters>(app_info.make_params());
}

} // namespace teleglax
