namespace teleglax {

template<>
void Teleglax::on_update(td::td_api::updateUser &update_user)
{
    auto user_ptr = user_db.insert(std::move(update_user.user_));
    chat_db.update_details(user_ptr->id_, user_ptr);
}

template<>
void Teleglax::on_update(td::td_api::updateUserStatus &uus)
{
    user_db.update_status(uus.user_id_, std::move(uus.status_));
}

template<>
void Teleglax::on_update(td::td_api::updateNewMessage &unm)
{
    event->on_new_message(Message::from_td_pointer(this, std::move(unm.message_)), unm.disable_notification_);
}

template<>
void Teleglax::on_update(td::td_api::updateMessageContent &update)
{
}


template<>
void Teleglax::on_update(td::td_api::updateDeleteMessages&)
{
}

template<>
void Teleglax::on_update(td::td_api::updateMessageEdited&)
{
}

template<>
void Teleglax::on_update(td::td_api::updateChatDraftMessage&)
{
}

template<>
void Teleglax::on_update(td::td_api::updateUserChatAction&)
{
}

template<>
void Teleglax::on_update(td::td_api::updateUnreadMessageCount&)
{
}

template<>
void Teleglax::on_update(td::td_api::updateNotificationSettings&)
{
}

template<>
void Teleglax::on_update(td::td_api::updateConnectionState& update)
{
    event->on_connection_status(EventTrigger::ConnectionStatus(update.state_->get_id()));
}

template<>
void Teleglax::on_update(td::td_api::ok&)
{
}

template<>
void Teleglax::on_update(td::td_api::updateRecentStickers&)
{
}

template<>
void Teleglax::on_update(td::td_api::updateOption& update)
{
    tg_options.insert({update.name_, *update.value_});
}

template<>
void Teleglax::on_update(td::td_api::updateMessageSendSucceeded&)
{
}

template<>
void Teleglax::on_update(td::td_api::updateSavedAnimations&)
{
}

template<>
void Teleglax::on_update(td::td_api::updateFile&)
{
}

template<>
void Teleglax::on_update(td::td_api::updateMessageMentionRead&)
{
}

template<>
void Teleglax::on_update(td::td_api::updateChatUnreadMentionCount&)
{
}

template<>
void Teleglax::on_update(td::td_api::updateInstalledStickerSets&)
{
}

template<>
void Teleglax::on_update(td::td_api::updateFavoriteStickers&)
{
}

template<>
void Teleglax::on_update(td::td_api::updateFileGenerationStart& update)
{
    file_generation.start_generation(
        this,
        update.generation_id_,
        update.conversion_,
        update.original_path_,
        update.destination_path_
    );
}

template<>
void Teleglax::on_update(td::td_api::updateFileGenerationStop& update)
{
    file_generation.stop_generation(update.generation_id_);
}

template<>
void Teleglax::on_update(td::td_api::updateUserFullInfo& update)
{
    chat_db.update_full_info(update.user_id_, std::move(update.user_full_info_));
}

} // namespace teleglax
