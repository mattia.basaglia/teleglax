namespace teleglax {

template<>
void Teleglax::on_update(td::td_api::updateNewChat &update_new_chat)
{
    auto& c = chat_db.insert(std::move(update_new_chat.chat_));
    event->on_new_chat(Chat(this, c.chat->id_));
}

template<>
void Teleglax::on_update(td::td_api::updateChatTitle &update)
{
    chat_db.update_title(update.chat_id_, update.title_);
    event->on_chat_changed(Chat(this, update.chat_id_));
}

template<>
void Teleglax::on_update(td::td_api::updateChatReadOutbox& update)
{
    chat_db.update_last_read_outbox(update.chat_id_, update.last_read_outbox_message_id_);
    event->on_chat_changed(Chat(this, update.chat_id_));
}

template<>
void Teleglax::on_update(td::td_api::updateChatReadInbox& update)
{
    chat_db.update_last_read_inbox(update.chat_id_, update.last_read_inbox_message_id_);
    event->on_chat_changed(Chat(this, update.chat_id_));
}

template<>
void Teleglax::on_update(td::td_api::updateChatOrder& update)
{
    chat_db.update_order(update.chat_id_, update.order_);
    event->on_chat_changed(Chat(this, update.chat_id_));
}

template<>
void Teleglax::on_update(td::td_api::updateBasicGroup& update)
{
    auto id = update.basic_group_->id_;
    auto chat_id = chat_db.update_details(id, std::move(update.basic_group_));
    if ( chat_id )
        event->on_chat_changed(Chat(this, chat_id));
}

template<>
void Teleglax::on_update(td::td_api::updateSupergroup& update)
{
    auto id = update.supergroup_->id_;
    auto chat_id = chat_db.update_details(id, std::move(update.supergroup_));
    if ( chat_id )
        event->on_chat_changed(Chat(this, chat_id));
}

template<>
void Teleglax::on_update(td::td_api::updateChatLastMessage& update)
{
    chat_db.update_last_message(update.chat_id_, std::move(update.last_message_));
    chat_db.update_order(update.chat_id_, update.order_);
    event->on_chat_changed(Chat(this, update.chat_id_));
}

template<>
void Teleglax::on_update(td::td_api::updateChatPhoto& update)
{
    chat_db.update_photo(update.chat_id_, std::move(update.photo_));
    event->on_chat_changed(Chat(this, update.chat_id_));
}

template<>
void Teleglax::on_update(td::td_api::updateChatIsPinned& update)
{
    chat_db.update_is_pinned(update.chat_id_, update.is_pinned_);
    chat_db.update_order(update.chat_id_, update.order_);
    event->on_chat_changed(Chat(this, update.chat_id_));
}

template<>
void Teleglax::on_update(td::td_api::updateChatReplyMarkup& update)
{
    chat_db.update_reply_markup(update.chat_id_, update.reply_markup_message_id_);
    event->on_chat_changed(Chat(this, update.chat_id_));
}

template<>
void Teleglax::on_update(td::td_api::updateSupergroupFullInfo& update)
{
    chat_db.update_full_info(update.supergroup_id_, std::move(update.supergroup_full_info_));
    event->on_chat_changed(Chat(this, update.supergroup_id_));
}

template<>
void Teleglax::on_update(td::td_api::updateBasicGroupFullInfo& update)
{
    chat_db.update_full_info(update.basic_group_id_, std::move(update.basic_group_full_info_));
    event->on_chat_changed(Chat(this, update.basic_group_id_));
}

} // namespace teleglax
