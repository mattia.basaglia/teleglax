#ifndef TELEGLAX_EXCEPTIONS_HPP
#define TELEGLAX_EXCEPTIONS_HPP

#include <exception>
#include <cstdint>

#include <td/telegram/td_api.h>

namespace teleglax {

class ApiError : public std::runtime_error
{
public:
    ApiError(const td::td_api::error& error)
        : std::runtime_error(error.message_),
          code(error.code_)
    {}

    std::int32_t error_code() const
    {
        return code;
    }

private:
    std::int32_t code;
};

class UnknownObjectError : public std::logic_error
{
public:
    UnknownObjectError(const td::td_api::Object& obj, std::string type)
        : std::logic_error(
            "Unknown " + type + ": " + to_string(obj)
        ), obj_id(obj.get_id())
    {}

    std::int32_t object_type_id() const { return obj_id; }

private:
    std::int32_t obj_id;
};

} // namespace teleglax

#endif // TELEGLAX_EXCEPTIONS_HPP
