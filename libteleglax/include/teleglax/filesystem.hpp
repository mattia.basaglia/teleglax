#ifndef TELEGLAX_FILESYSTEM
#define TELEGLAX_FILESYSTEM

#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>

#include <experimental/filesystem>

namespace teleglax {

namespace fs = std::experimental::filesystem;

inline fs::path home()
{
    static const char *homedir = nullptr;
    if ( !homedir )
    {
        struct passwd *pw = getpwuid(getuid());
        homedir = pw->pw_dir;
    }
    return homedir;
}

} // namespace teleglax
#endif // TELEGLAX_FILESYSTEM
