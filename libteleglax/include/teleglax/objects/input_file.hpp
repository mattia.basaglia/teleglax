#ifndef TELEGLAX_OBJECTS_INPUT_FILE_HPP
#define TELEGLAX_OBJECTS_INPUT_FILE_HPP

#include <mutex>
#include <string>
#include <future>
#include <fstream>
#include <functional>
#include <unordered_map>
#include <unordered_set>

#include <td/telegram/td_api.h>

#include "../filesystem.hpp"

namespace teleglax {

class InputFile
{
public:
    enum class Type
    {
        Id,             // File by local ID
        Remote,         // File by remote ID
        Path,           // File by path
        Url,            // File by remote Url
        Custom,         // File by custom callback, path is in the format
                        // "conversion:path" where "conversion" is the name of
                        // a registered callback in CustomFileGeneration and
                        // "path" is passed to the callback when file generation is needed
    };

    InputFile(Type type, std::string path)
        : type_(type), path_(std::move(path))
    {}

    explicit InputFile(const fs::path& path)
        : type_(Type::Path), path_(path.string())
    {}

    Type type() const
    {
        return type_;
    }

    const std::string& path() const
    {
        return path_;
    }

    td::td_api::object_ptr<td::td_api::InputFile> to_td() const
    {
        using namespace td::td_api;
        switch ( type_ )
        {
            case Type::Id:
                return make_object<inputFileId>(std::stol(path_));
            case Type::Remote:
                return make_object<inputFileRemote>(path_);
            case Type::Path:
            default:
                return make_object<inputFileLocal>(path_);
            case Type::Url:
                return make_object<inputFileGenerated>(path_, "#url#", 0);
            case Type::Custom:
            {
                auto pos = std::find(path_.begin(), path_.end(), ':');
                std::string conversion(path_.begin(), pos);
                std::string path(pos == path_.end() ? path_.end() : pos + 1, path_.end());
                return make_object<inputFileGenerated>(std::move(path), std::move(conversion), 0);
            }
        }
    }

private:
    Type type_;
    std::string path_;
};

class FileGeneratorError : public std::runtime_error
{
public:
    FileGeneratorError(const std::string& what, const std::string& generation, const std::string& path)
        : std::runtime_error(what),
          generation(generation),
          path(path)
    {}

    std::string generation;
    std::string path;

};

class Teleglax;

class CustomFileGeneration
{
public:
    using function_type = std::function<void (std::ostream& out, const std::string& path)>;

    template<class Callable>
    void register_callback(const std::string& generation, Callable&& callback)
    {
        map.insert({generation, std::forward<Callable>(callback)});
    }

    void generate(
        Teleglax* teleglax,
        std::int64_t generation_id,
        const std::string& generation,
        const std::string& path,
        const std::string& output_filename
    );

    void start_generation(
        Teleglax* teleglax,
        std::int64_t generation_id,
        std::string generation,
        std::string path,
        std::string output_filename
    )
    {
        std::unique_lock lock{mutex};
        for ( auto it = async_finished.begin(); it != async_finished.end(); )
        {
            async_generations.erase(*it);
            it = async_finished.erase(it);
        }

        async_generations.insert({
            generation_id,
            std::async([=](){
                generate(teleglax, generation_id, generation, path, output_filename);
                std::unique_lock lock{mutex};
                async_finished.insert(generation_id);
            })
        });
    }

    void stop_generation(std::int64_t generation_id)
    {
        std::unique_lock lock{mutex};
        auto iter = async_generations.find(generation_id);
        if ( iter == async_generations.end() )
            return;

        auto iter_fin = async_finished.find(generation_id);
        if ( iter_fin != async_finished.end() )
        {
            iter->second.get();
            async_finished.erase(iter_fin);
        }

        async_generations.erase(iter);
    }

private:
    std::unordered_map<std::string, function_type> map;
    std::unordered_map<std::int64_t, std::future<void>> async_generations;
    std::unordered_set<std::int64_t> async_finished;
    std::mutex mutex;
};

} // namespace teleglax
#endif // TELEGLAX_OBJECTS_INPUT_FILE_HPP
