#ifndef TELEGLAX_OBJECTS_CHAT_HPP
#define TELEGLAX_OBJECTS_CHAT_HPP

#include "wrapper.hpp"
#include "file.hpp"

namespace teleglax {
class Message;
class User;
class MessageContent;
class ChatMember;

namespace detail {

enum class SearchFilter
{
    Empty = td::td_api::searchMessagesFilterEmpty::ID,
    Animation = td::td_api::searchMessagesFilterAnimation::ID,
    Audio = td::td_api::searchMessagesFilterAudio::ID,
    Document = td::td_api::searchMessagesFilterDocument::ID,
    Photo = td::td_api::searchMessagesFilterPhoto::ID,
    Video = td::td_api::searchMessagesFilterVideo::ID,
    VoiceNote = td::td_api::searchMessagesFilterVoiceNote::ID,
    PhotoAndVideo = td::td_api::searchMessagesFilterPhotoAndVideo::ID,
    Url = td::td_api::searchMessagesFilterUrl::ID,
    ChatPhoto = td::td_api::searchMessagesFilterChatPhoto::ID,
    Call = td::td_api::searchMessagesFilterCall::ID,
    MissedCall = td::td_api::searchMessagesFilterMissedCall::ID,
    VideoNote = td::td_api::searchMessagesFilterVideoNote::ID,
    VoiceAndVideoNote = td::td_api::searchMessagesFilterVoiceAndVideoNote::ID,
    Mention = td::td_api::searchMessagesFilterMention::ID,
    UnreadMention = td::td_api::searchMessagesFilterUnreadMention::ID,
};

template<class C, class D> void on_advance(C* obj, const std::vector<D>& result)
{
    auto& target = obj->chronological ? result.front() : result.back();
    obj->from_message_id = target.id();
}

template<class Derived>
class GetMessagesCallback : public Fetcher<Derived, Message, td::td_api::messages>
{
public:
    bool receive(td::td_api::object_ptr<td::td_api::messages> rmessages);
    void prepare_result();
    void on_advance(const std::vector<Message>& result)
    {
        detail::on_advance(this, result);
    }

    std::int64_t chat_id;
    std::int32_t after;
    std::int32_t offset;
    std::int64_t from_message_id;
    bool chronological;
};

class GetMessages : public GetMessagesCallback<GetMessages>
{
public:
    void send_request();

    bool local_only;
};


class SearchMessages : public GetMessagesCallback<SearchMessages>
{
public:
    void send_request();
    td::td_api::object_ptr<td::td_api::SearchMessagesFilter> make_filter() const;

    std::string query;
    std::int32_t sender_user_id;
    SearchFilter filter;
};


extern template void on_advance<GetMessages, Message>(GetMessages* obj, const std::vector<Message>& result);
extern template void on_advance<SearchMessages, Message>(SearchMessages* obj, const std::vector<Message>& result);
} // namespace detail

class Chat : public BaseWrapper<Chat, td::td_api::chat>
{
public:
    enum class Type
    {
        Private = td::td_api::chatTypePrivate::ID,
        BasicGroup = td::td_api::chatTypeBasicGroup::ID,
        Supergroup = td::td_api::chatTypeSupergroup::ID,
        Secret = td::td_api::chatTypeSecret::ID,
        Unknown = td::td_api::userTypeUnknown::ID,
    };

    using SearchFilter = detail::SearchFilter;

    using BaseWrapper::BaseWrapper;

    void refresh()
    {
        auto it = source->chats().find(id_);
        if ( it != source->chats().end() )
        {
            wrapped = it->second.chat;
            details = it->second.details;
            full_info = it->second.full_info;
        }
        else
        {
            wrapped = source->request<td::td_api::getChat>(id_).get();
            details = {};
            full_info = {};
        }
    }

    auto type() const { return get_value_id(&wrapped_type::type_, Type::Unknown); }
    auto title() const { return get_value(&wrapped_type::title_, "unknown chat"); }
    /// \todo  notification_settings_ draft_message_
    auto order() const { return get_value(&wrapped_type::order_, std::numeric_limits<std::int64_t>::max()); }
    auto is_pinned() const { return get_value(&wrapped_type::is_pinned_); }
    auto can_be_reported() const { return get_value(&wrapped_type::can_be_reported_); }
    auto unread_count() const { return get_value(&wrapped_type::unread_count_); }
    auto unread_mention_count() const { return get_value(&wrapped_type::unread_mention_count_); }
    auto client_data() const { return get_value(&wrapped_type::client_data_); }
    Message last_read_inbox() const;
    Message last_read_outbox() const;
    Message reply_markup_message() const;
    bool has_chat_image() const
    {
        return ensure_object() && wrapped->photo_;
    }
    File chat_image_large() const
    {
        if ( !has_chat_image() )
            return {source, 0};
        return File{source, wrapped->photo_->big_->id_, {wrapped, wrapped->photo_->big_.get()}};
    }
    File chat_image_small() const
    {
        if ( !has_chat_image() )
            return {source, 0};
        return File{source, wrapped->photo_->small_->id_, {wrapped, wrapped->photo_->small_.get()}};
    }
    Message last_message() const;

    std::future<Message> pinned_message() const;

    /**
     * \brief Get chat history
     * \param before            Number of messages before \p from_message_id to return
     * \param after             Number of messages after \p from_message_id to return
     * \param from_message_id   ID of the message to use as reference, 0 means the end of the chat
     * \param local_only        Return available messages without making network requests
     * \param chronological     Whether to sort results in chronological or reverse chronological order
     * \return Vector of messages
     **/
    std::future<std::vector<Message>> get_history(
        std::int32_t before,
        std::int32_t after = 0,
        std::int64_t from_message_id = 0,
        bool local_only = false,
        bool chronological = true
    ) const;

    std::future<std::vector<Message>> search(
        std::string query,
        std::int32_t before,
        std::int32_t after = 0,
        std::int64_t from_message_id = 0,
        std::int32_t sender_user_id = 0,
        SearchFilter filter = SearchFilter::Empty,
        bool chronological = true
    ) const;

    /**
     * \brief History iterable for messages in reverse chronological order
     */
    FetchRange<detail::GetMessages> history_range(
        std::int64_t from_message_id = 0,
        bool local_only = false
    ) const;

    /**
     * \brief Search iterable for messages in reverse chronological order
     */
    FetchRange<detail::SearchMessages> search_range(
        std::string query,
        std::int64_t from_message_id = 0,
        std::int32_t sender_user_id = 0,
        SearchFilter filter = SearchFilter::Empty
    ) const;

    bool is_channel() const
    {
        if ( auto sg = supergroup_details() )
            return sg->is_channel_;
        return false;
    }

    std::string username() const
    {
        if ( auto sg = supergroup_details() )
            return sg->username_;
        if ( auto u = user_details() )
            return u->username_;
        return {};
    }

    std::int32_t member_count() const
    {
        if ( type() == Type::Supergroup )
        {
            if ( details && supergroup_details()->member_count_ )
                return supergroup_details()->member_count_;
            return supergroup_full_info()->member_count_;
        }
        else if ( type() == Type::BasicGroup )
        {
            return basic_group_details()->member_count_;
        }
        return 2;
    }

    bool anyone_can_invite() const
    {
        if ( auto sg = supergroup_details() )
            return sg->anyone_can_invite_;

        if ( type() == Type::BasicGroup )
            return true;

        return false;
    }

    bool sign_messages() const
    {
        if ( auto sg = supergroup_details() )
            return sg->sign_messages_;
        return false;
    }

    bool is_verified() const
    {
        if ( auto sg = supergroup_details() )
            return sg->is_verified_;
        if ( auto u = user_details() )
            return u->is_verified_;
        return {};
    }

    std::string restriction_reason() const
    {
        if ( auto sg = supergroup_details() )
            return sg->restriction_reason_;
        if ( auto u = user_details() )
            return u->restriction_reason_;
        return {};
    }

    std::string description() const
    {
        if ( auto sg = supergroup_full_info() )
            return sg->description_;
        if ( auto u = user_full_info() )
            return u->bio_;
        return {};
    }

    bool can_get_members() const
    {
        if ( auto sg = supergroup_full_info() )
            return sg->can_get_members_;
        return true;
    }

    std::string invite_link() const
    {
        if ( auto sg = supergroup_full_info() )
            return sg->invite_link_;
        if ( auto bg = basic_group_full_info() )
            return bg->invite_link_;
        if ( auto u = user_details() )
        {
            std::string base = source->option("t_me_url").as_string();
            if ( !u->username_.empty() && !base.empty() )
                return base + u->username_;
        }
        return {};
    }

    bool everyone_is_administrator() const
    {
        if ( auto bg = basic_group_details() )
            return bg->everyone_is_administrator_;
        return false;
    }

    /// \brief If the chat is a private/secret chat, returns the associated user
    User user() const;

    /// \todo reply_markup_
    std::future<Message> send_message(
        MessageContent content,
        bool disable_notification = false,
        bool from_background = false,
        std::int64_t reply_to_message_id = 0
    ) const;

    std::future<std::vector<ChatMember>> chat_members() const;

private:
    mutable std::shared_ptr<td::td_api::Object> details;
    mutable std::shared_ptr<td::td_api::Object> full_info;

    auto related_id() const
    {
        return ChatData::td_related_id(wrapped);
    }

    template<class FuncT, Type match_type, std::shared_ptr<td::td_api::Object> (Chat::*memptr)>
    typename FuncT::ReturnType::element_type* get_details() const
    {
        ensure_object();
        if ( type() != match_type )
            return nullptr;

        if ( !(this->*memptr) )
        {
            auto id = related_id();
            if ( !id )
                return nullptr;
            const_cast<Chat*>(this)->*memptr = source->request<FuncT>(id).get();
        }

        return this->*memptr ? static_cast<typename FuncT::ReturnType::element_type*>((this->*memptr).get()) : nullptr;
    }

    /// \todo date_ status_
    td::td_api::supergroup* supergroup_details() const
    {
        return get_details<td::td_api::getSupergroup, Type::Supergroup, &Chat::details>();
    }

    /// \todo status_ is_active_ upgraded_to_supergroup_id_
    td::td_api::basicGroup* basic_group_details() const
    {
        return get_details<td::td_api::getBasicGroup, Type::BasicGroup, &Chat::details>();
    }

    td::td_api::user* user_details() const
    {
        if ( type() == Type::Private )
            return get_details<td::td_api::getUser, Type::Private, &Chat::details>();
        return get_details<td::td_api::getUser, Type::Secret, &Chat::details>();
    }

    /// \todo administrator_count_ restricted_count_ can_set_username_
    ///       can_set_sticker_set_ sticker_set_id_
    ///       upgraded_from_basic_group_id_ upgraded_from_max_message_id_
    td::td_api::supergroupFullInfo* supergroup_full_info() const
    {
        return get_details<td::td_api::getSupergroupFullInfo, Type::Supergroup, &Chat::full_info>();
    }

    /// \todo creator_user_id_
    td::td_api::basicGroupFullInfo* basic_group_full_info() const
    {
        return get_details<td::td_api::getBasicGroupFullInfo, Type::BasicGroup, &Chat::full_info>();
    }

    td::td_api::userFullInfo* user_full_info() const
    {
        if ( type() == Type::Private )
            return get_details<td::td_api::getUserFullInfo, Type::Private, &Chat::full_info>();
        return get_details<td::td_api::getUserFullInfo, Type::Secret, &Chat::full_info>();
    }
};

} // namespace teleglax
#endif // TELEGLAX_OBJECTS_CHAT_HPP
