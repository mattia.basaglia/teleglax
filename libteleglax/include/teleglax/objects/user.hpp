#ifndef TELEGLAX_OBJECTS_USER_HPP
#define TELEGLAX_OBJECTS_USER_HPP

#include <vector>
#include <future>

#include "wrapper.hpp"
#include "file.hpp"

namespace teleglax {

class ProfilePhoto : public BaseWrapper<ProfilePhoto, td::td_api::profilePhoto>
{
public:
    using BaseWrapper::BaseWrapper;

    void refresh()
    {
    }

    File large() const
    {
        if ( !ensure_object() )
            return {source, 0};
        return File{source, wrapped->big_->id_, {wrapped, wrapped->big_.get()}};
    }
    File small() const
    {
        if ( !ensure_object() )
            return {source, 0};
        return File{source, wrapped->small_->id_, {wrapped, wrapped->small_.get()}};
    }
};

class Chat;

class User : public BaseWrapper<User, td::td_api::user>
{
public:
    enum class Type
    {
        Regular = td::td_api::userTypeRegular::ID,
        Deleted = td::td_api::userTypeDeleted::ID,
        Bot = td::td_api::userTypeBot::ID,
        Unknown = td::td_api::userTypeUnknown::ID,
    };

    enum class LinkState
    {
        None = td::td_api::linkStateNone::ID,
        KnowsPhoneNumber = td::td_api::linkStateKnowsPhoneNumber::ID,
        IsContact = td::td_api::linkStateIsContact::ID,
    };

    enum class Status
    {
        Empty = td::td_api::userStatusEmpty::ID,
        Online = td::td_api::userStatusOnline::ID,
        Offline = td::td_api::userStatusOffline::ID,
        Recently = td::td_api::userStatusRecently::ID,
        LastWeek = td::td_api::userStatusLastWeek::ID,
        LastMonth = td::td_api::userStatusLastMonth::ID,
    };

    using BaseWrapper::BaseWrapper;

    void refresh()
    {
        wrapped = source->users()[id_];
        if ( !wrapped )
            wrapped = std::move(source->get_user(id_).get().wrapped);
    }

    std::string first_name() const { return get_value(&wrapped_type::first_name_, "unknown user"); }
    std::string last_name() const { return get_value(&wrapped_type::last_name_); }
    std::string full_name() const
    {
        std::string name = first_name();
        std::string last = last_name();
        if ( !last.empty() && !name.empty() )
            name += ' ';
        name += last;
        return name;
    }
    std::string username() const { return get_value(&wrapped_type::username_); }
    std::string phone_number() const { return get_value(&wrapped_type::phone_number_); }
    bool is_verified() const { return get_value(&wrapped_type::is_verified_); }
    std::string restriction_reason() const { return get_value(&wrapped_type::restriction_reason_); }
    bool have_access() const { return get_value(&wrapped_type::have_access_); }
    Type type() const { return get_value_id(&wrapped_type::type_, Type::Unknown); }
    std::string language_code() const { return get_value(&wrapped_type::language_code_, ""); }
    LinkState outgoing_link() const  { return get_value_id(&wrapped_type::outgoing_link_, LinkState::None); }
    LinkState incoming_link() const  { return get_value_id(&wrapped_type::incoming_link_, LinkState::None); }
    Status status() const { return get_value_id(&wrapped_type::status_, Status::Empty); }
    std::chrono::system_clock::time_point online_at() const
    {
        Status status = this->status();

        if ( status == Status::Online )
            return  detail::convert_date(
                static_cast<td::td_api::userStatusOnline&>(*wrapped->status_).expires_
            );
        if ( status == Status::Offline )
            return  detail::convert_date(
                static_cast<td::td_api::userStatusOffline&>(*wrapped->status_).was_online_
            );

        return {};
    }

    bool has_profile_photo() const
    {
        return ensure_object() && wrapped->profile_photo_;
    }

    ProfilePhoto profile_photo() const
    {
        if ( !has_profile_photo() )
            return {source, 0};
        return {source, wrapped->profile_photo_->id_, {wrapped, wrapped->profile_photo_.get()}};
    }

    std::future<void> block() const
    {
        return source->request_void<td::td_api::blockUser>(id_);
    }

    std::future<std::vector<Chat>> groups_in_common() const;
};

} // namespace teleglax
#endif // TELEGLAX_OBJECTS_USER_HPP
