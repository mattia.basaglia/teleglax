#ifndef TELEGLAX_MESSAGE_CONTENT_HPP
#define TELEGLAX_MESSAGE_CONTENT_HPP

#include <variant>

#include "wrapper.hpp"
#include "file.hpp"
#include "input_file.hpp"

namespace teleglax {
class FormattedText
{
public:
    /// \todo entities
    FormattedText(std::string text={})
        : text(text)
    {}

    FormattedText(const FormattedText&) = default;
    FormattedText(FormattedText&&) = default;
    FormattedText& operator=(const FormattedText&) = default;
    FormattedText& operator=(FormattedText&&) = default;

    FormattedText& operator=(const std::string& str)
    {
        text = str;
        return *this;
    }

    void append(const std::string& str)
    {
        text += str;
    }

    const std::string& str() const
    {
        return text;
    }

    FormattedText& operator+=(const std::string& str)
    {
        text += str;
        return *this;
    }

    td::td_api::object_ptr<td::td_api::formattedText> to_td() const
    {
        using namespace td::td_api;
        return make_object<formattedText>(text, std::vector<object_ptr<textEntity>>{});
    }

private:
    std::string text;
};


class MessageContentBase
{
public:
    enum class Type
    {
        Text = td::td_api::messageText::ID,
        Animation = td::td_api::messageAnimation::ID,
        Audio = td::td_api::messageAudio::ID,
        Document = td::td_api::messageDocument::ID,
        Photo = td::td_api::messagePhoto::ID,
        ExpiredPhoto = td::td_api::messageExpiredPhoto::ID,
        Sticker = td::td_api::messageSticker::ID,
        Video = td::td_api::messageVideo::ID,
        ExpiredVideo = td::td_api::messageExpiredVideo::ID,
        VideoNote = td::td_api::messageVideoNote::ID,
        VoiceNote = td::td_api::messageVoiceNote::ID,
        Location = td::td_api::messageLocation::ID,
        Venue = td::td_api::messageVenue::ID,
        Contact = td::td_api::messageContact::ID,
        Game = td::td_api::messageGame::ID,
        Invoice = td::td_api::messageInvoice::ID,
        Call = td::td_api::messageCall::ID,
        BasicGroupChatCreate = td::td_api::messageBasicGroupChatCreate::ID,
        SupergroupChatCreate = td::td_api::messageSupergroupChatCreate::ID,
        ChatChangeTitle = td::td_api::messageChatChangeTitle::ID,
        ChatChangePhoto = td::td_api::messageChatChangePhoto::ID,
        ChatDeletePhoto = td::td_api::messageChatDeletePhoto::ID,
        ChatAddMembers = td::td_api::messageChatAddMembers::ID,
        ChatJoinByLink = td::td_api::messageChatJoinByLink::ID,
        ChatDeleteMember = td::td_api::messageChatDeleteMember::ID,
        ChatUpgradeTo = td::td_api::messageChatUpgradeTo::ID,
        ChatUpgradeFrom = td::td_api::messageChatUpgradeFrom::ID,
        PinMessage = td::td_api::messagePinMessage::ID,
        ScreenshotTaken = td::td_api::messageScreenshotTaken::ID,
        ChatSetTtl = td::td_api::messageChatSetTtl::ID,
        CustomServiceAction = td::td_api::messageCustomServiceAction::ID,
        GameScore = td::td_api::messageGameScore::ID,
        PaymentSuccessful = td::td_api::messagePaymentSuccessful::ID,
        PaymentSuccessfulBot = td::td_api::messagePaymentSuccessfulBot::ID,
        ContactRegistered = td::td_api::messageContactRegistered::ID,
        WebsiteConnected = td::td_api::messageWebsiteConnected::ID,
        Unsupported = td::td_api::messageUnsupported::ID,
    };

    virtual ~MessageContentBase() = default;

    virtual td::td_api::object_ptr<td::td_api::InputMessageContent> to_td_input() const = 0;
    virtual Type type() const { return Type::Unsupported; }
};


namespace msg {
using Type = MessageContentBase::Type;

template<Type t>
    class TypedMessageContent : public MessageContentBase
    {
    public:
        static constexpr Type static_type = t;
        static constexpr bool owns_type(Type type) noexcept
        {
            return type == static_type;
        }
        Type type() const override { return static_type; }
    };

class Text : public TypedMessageContent<Type::Text>
{
public:
    Text(std::string text)
        : text_(std::move(text))
    {}

    Text(FormattedText text={})
        : text_(std::move(text))
    {}

    td::td_api::object_ptr<td::td_api::InputMessageContent> to_td_input() const override
    {
        /// \todo disable_web_page_preview_ clear_draft_
        return td::td_api::make_object<td::td_api::inputMessageText>(
            text_.to_td(),
            false,
            false
        );
    }

    const FormattedText& text() const { return text_; }
    FormattedText& text() { return text_; }

private:
    /// \todo web_page_
    FormattedText text_;
};

class Media
{
public:
    static constexpr bool owns_type(Type type) noexcept
    {
        return type == Type::Animation || type == Type::Audio ||
               type == Type::Document || type == Type::Photo ||
               type == Type::Sticker || type == Type::Video ||
               type == Type::VideoNote || type == Type::VoiceNote
        ;
    }
    Media() = default;
    explicit Media(FormattedText caption)
        : caption_(std::move(caption)) {}
    explicit Media(File file, FormattedText caption = {})
        : file_(std::move(file)),
          caption_(std::move(caption))
    {}
    explicit Media(InputFile file, FormattedText caption = {})
        : file_(std::move(file)),
          caption_(std::move(caption))
    {}

    bool is_input() const
    {
        return file_.index() == 2;
    }

    bool is_output() const
    {
        return file_.index() == 1;
    }

    bool is_null() const
    {
        return file_.index() == 0;
    }

    const FormattedText& caption() const
    {
        return caption_;
    }

    FormattedText& caption()
    {
        return caption_;
    }

    void set_input(InputFile file)
    {
        file_ = std::move(file);
    }

    void set_output(File file)
    {
        file_ = std::move(file);
    }

protected:
    /// \pre !is_null()
    InputFile to_input_file() const
    {
        if ( is_input() )
            return std::get<2>(file_);
        const File& file = std::get<1>(file_);
        if ( file.remote_id().empty() )
            return InputFile(InputFile::Type::Id, std::to_string(file.id()));
        return InputFile(InputFile::Type::Remote, file.remote_id());
    }

    std::variant<std::monostate, File, InputFile> file_;
    FormattedText caption_;
    /// \todo thumbnail
};

class GraphicMedia : virtual public Media
{
public:
    using Media::Media;

    static constexpr bool owns_type(Type type) noexcept
    {
        return type == Type::Animation || type == Type::Photo ||
               type == Type::Sticker || type == Type::Video ||
               type == Type::VideoNote
        ;
    }

    void set_width(std::int32_t width) { width_ = width; }
    std::int32_t width() const { return width_; }
    void set_height(std::int32_t height) { height_ = height; }
    std::int32_t height() const { return height_; }

protected:
    std::int32_t width_ = 0;
    std::int32_t height_ = 0;
};


class PlayableMedia : virtual public Media
{
public:
    using Media::Media;

    static constexpr bool owns_type(Type type) noexcept
    {
        return type == Type::Animation || type == Type::Audio ||
               type == Type::Video ||
               type == Type::VideoNote || type == Type::VoiceNote
        ;
    }

    void set_duration(std::int32_t duration) { duration_ = duration; }
    std::int32_t duration() const { return duration_; }

protected:
    std::int32_t duration_ = 0;
};

class Sticker : public GraphicMedia, public TypedMessageContent<Type::Sticker>
{
public:
    using TypedMessageContent::owns_type;
    using GraphicMedia::GraphicMedia;

    void set_set_id(std::int64_t set_id) { set_id_ = set_id; }
    std::int64_t set_id() const { return set_id_; }
    void set_emoji(const std::string& emoji) { emoji_ = emoji; }
    const std::string& emoji() const { return emoji_; }

    td::td_api::object_ptr<td::td_api::InputMessageContent> to_td_input() const override
    {
        using namespace td::td_api;
        if ( is_null() )
            return {};

        return make_object<inputMessageSticker>(
            to_input_file().to_td(),
            std::unique_ptr<inputThumbnail>{},
            width_,
            height_
        );
    }

private:
    std::int64_t set_id_;
    std::string emoji_;

    /// \todo is_mask_ mask_position_
};

class Photo : public GraphicMedia, public TypedMessageContent<Type::Photo>
{
public:
    using TypedMessageContent::owns_type;
    using GraphicMedia::GraphicMedia;

    td::td_api::object_ptr<td::td_api::InputMessageContent> to_td_input() const override
    {
        using namespace td::td_api;
        if ( is_null() )
            return {};

        return make_object<inputMessagePhoto>(
            to_input_file().to_td(),
            std::unique_ptr<inputThumbnail>{},
            std::vector<std::int32_t>{},
            width_,
            height_,
            caption_.to_td(),
            0
        );
    }

    /// \todo added_sticker_file_ids_ ttl_ messagePhoto
};

class Document : public Media, public TypedMessageContent<Type::Document>
{
public:
    using TypedMessageContent::owns_type;
    using Media::Media;

    td::td_api::object_ptr<td::td_api::InputMessageContent> to_td_input() const override
    {
        using namespace td::td_api;
        if ( is_null() )
            return {};

        return make_object<inputMessageDocument>(
            to_input_file().to_td(),
            std::unique_ptr<inputThumbnail>{},
            caption_.to_td()
        );
    }

    /// \todo file_name_ mime_type_
};

class Animation : public GraphicMedia, public PlayableMedia, public TypedMessageContent<Type::Animation>
{
public:
    using TypedMessageContent::owns_type;
    using GraphicMedia::GraphicMedia;

    td::td_api::object_ptr<td::td_api::InputMessageContent> to_td_input() const override
    {
        using namespace td::td_api;
        if ( is_null() )
            return {};

        return make_object<inputMessageAnimation>(
            to_input_file().to_td(),
            std::unique_ptr<inputThumbnail>{},
            duration_,
            width_,
            height_,
            caption_.to_td()
        );
    }

    void set_duration(std::int32_t duration) { duration_ = duration; }
    std::int32_t duration() const { return duration_; }

private:
    /// \todo file_name_ mime_type_ is_secret_
};

class Audio : public PlayableMedia, public TypedMessageContent<Type::Audio>
{
public:
    using TypedMessageContent::owns_type;
    using PlayableMedia::PlayableMedia;

    td::td_api::object_ptr<td::td_api::InputMessageContent> to_td_input() const override
    {
        using namespace td::td_api;
        if ( is_null() )
            return {};

        return make_object<inputMessageAudio>(
            to_input_file().to_td(),
            std::unique_ptr<inputThumbnail>{},
            duration_,
            title_,
            performer_,
            caption_.to_td()
        );
    }

    void set_title(const std::string& title) { title_ = title; }
    const std::string& title() const { return title_; }

    void set_performer(const std::string& performer) { performer_ = performer; }
    const std::string& performer() const { return performer_; }

private:
    std::string title_;
    std::string performer_;
    /// \todo mime_type_
};

class Video : public GraphicMedia, public PlayableMedia, public TypedMessageContent<Type::Video>
{
public:
    using TypedMessageContent::owns_type;
    using GraphicMedia::GraphicMedia;

    td::td_api::object_ptr<td::td_api::InputMessageContent> to_td_input() const override
    {
        using namespace td::td_api;
        if ( is_null() )
            return {};

        return make_object<inputMessageVideo>(
            to_input_file().to_td(),
            std::unique_ptr<inputThumbnail>{},
            std::vector<std::int32_t>{},
            duration_,
            width_,
            height_,
            supports_streaming_,
            caption_.to_td(),
            0
        );
    }

    void set_supports_streaming(bool supports_streaming) { supports_streaming_ = supports_streaming; }
    bool supports_streaming() const { return supports_streaming_; }

private:
    /// \todo added_sticker_file_ids_ ttl_
    ///       is_secret_ file_name_ mime_type_ has_stickers_
    bool supports_streaming_ = false;
};

class VideoNote : public GraphicMedia, public PlayableMedia, public TypedMessageContent<Type::VideoNote>
{
public:
    using TypedMessageContent::owns_type;
    using GraphicMedia::GraphicMedia;

    td::td_api::object_ptr<td::td_api::InputMessageContent> to_td_input() const override
    {
        using namespace td::td_api;
        if ( is_null() )
            return {};

        return make_object<inputMessageVideoNote>(
            to_input_file().to_td(),
            std::unique_ptr<inputThumbnail>{},
            duration_,
            width_
        );
    }
    /// \todo is_viewed_ is_secret_
};

class VoiceNote : public PlayableMedia, public TypedMessageContent<Type::VoiceNote>
{
public:
    using TypedMessageContent::owns_type;
    using PlayableMedia::PlayableMedia;

    td::td_api::object_ptr<td::td_api::InputMessageContent> to_td_input() const override
    {
        using namespace td::td_api;
        if ( is_null() )
            return {};

        return make_object<inputMessageVoiceNote>(
            to_input_file().to_td(),
            duration_,
            waveform_,
            caption_.to_td()
        );
    }

    void set_waveform(const std::string& waveform) { waveform_ = waveform; }
    const std::string& waveform() const { return waveform_; }

private:
    /// \todo is_listened_ mime_type_
    std::string waveform_;
};

} // namespace msg


class MessageContent
{
public:
    using Type = MessageContentBase::Type;

    template<class T>
        MessageContent(T obj)
            : data(std::make_unique<T>(std::move(obj)))
        {}

    template<class T>
        MessageContent(std::unique_ptr<T> data)
        : data(std::move(data))
        {}

    MessageContent() noexcept = default;
    MessageContent(MessageContent&&) noexcept = default;
    MessageContent& operator=(MessageContent&&) noexcept = default;

    template<class T>
        MessageContent& operator=(T obj)
        {
            data = std::make_unique<T>(std::move(obj));
            return *this;
        }

    template<class T>
        MessageContent* downcast()
        {
            if ( data && T::owns_type(data->type()) )
                return static_cast<T*>(data.get());
            return nullptr;
        }

    td::td_api::object_ptr<td::td_api::InputMessageContent> to_td_input() const
    {
        return data ? data->to_td_input() : msg::Text().to_td_input();
    }

    Type type() const
    {
        return data ? data->type() : Type::Unsupported;
    }

    explicit operator bool() const
    {
        return bool(data);
    }

private:
    std::unique_ptr<MessageContentBase> data;
};

} // namespace teleglax
#endif // TELEGLAX_MESSAGE_CONTENT_HPP
