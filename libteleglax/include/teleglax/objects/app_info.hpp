#ifndef TELEGLAX_APP_INFO_HPP
#define TELEGLAX_APP_INFO_HPP

#include <string>


#include <td/telegram/td_api.h>

#include "../filesystem.hpp"

namespace teleglax {

struct AppInfo
{
// application
    /// from https://my.telegram.org/
    std::int32_t api_id = 0;
    /// from https://my.telegram.org/
    std::string api_hash;
    /// must be non-empty
    std::string application_version = "1.0";

// features
    bool use_test_environment = false;
    bool use_secret_chats = false;

// storage
    /// path to the database storage (empty = current directory)
    fs::path database_directory;
    /// path to the file storage (empty = database_directory)
    fs::path files_directory;
    bool use_file_database = false;
    bool use_chat_info_database = false;
    bool use_message_database = false;
    bool enable_storage_optimizer = true;
    /// If set to true, original file names will be ignored.
    bool ignore_file_names = false;

// system
    std::string system_language_code = "en";
    std::string device_model = "Desktop";
    /// operating system version
    std::string system_version = "unknown";

    td::td_api::object_ptr<td::td_api::tdlibParameters> make_params() const
    {
        return td::td_api::make_object<td::td_api::tdlibParameters>(
            use_test_environment,
            database_directory,
            files_directory,
            use_file_database,
            use_chat_info_database,
            use_message_database,
            use_secret_chats,
            api_id,
            api_hash,
            system_language_code,
            device_model,
            system_version,
            application_version,
            enable_storage_optimizer,
            ignore_file_names
        );
    }
};

} // namespace teleglax
#endif // TELEGLAX_APP_INFO_HPP
