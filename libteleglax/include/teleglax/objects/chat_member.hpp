#ifndef TELEGLAX_OBJECTS_CHAT_MEMBER_HPP
#define TELEGLAX_OBJECTS_CHAT_MEMBER_HPP

#include "user.hpp"

namespace teleglax {


class BotInfo : public UnfetchableWrapper<BotInfo, td::td_api::botInfo>
{
public:
    class Command : public UnfetchableWrapper<Command, td::td_api::botCommand>
    {
    public:
       using UnfetchableWrapper::UnfetchableWrapper;
        auto command() const { return get_value(&wrapped_type::command_); }
        auto description() const { return get_value(&wrapped_type::description_); }
    };

private:
    template<class BaseIter>
    class IteratorWrapper
    {
    public:
        using value_type = Command;
        using difference_type = typename std::iterator_traits<BaseIter>::difference_type;
        using reference = const Command&;
        using pointer = const Command*;
        using iterator_category = typename std::iterator_traits<BaseIter>::iterator_category;

        IteratorWrapper() : IteratorWrapper({}, nullptr) {}

        reference operator*() const
        {
            cache = deref();
            return cache;
        }
        pointer operator->() const
        {
            cache = deref();
            return &cache;
        }

        IteratorWrapper& operator++()
        {
            ++iter;
            return this;
        }

        IteratorWrapper operator++(int)
        {
            auto copy = *this;
            ++*this;
            return copy;
        }

        IteratorWrapper& operator--()
        {
            --iter;
            return this;
        }


        IteratorWrapper operator--(int)
        {
            auto copy = *this;
            --*this;
            return copy;
        }

        bool operator==(const IteratorWrapper& oth) const { return iter == oth.iter; }
        bool operator!=(const IteratorWrapper& oth) const { return iter == oth.iter; }
        bool operator<(const IteratorWrapper& oth) const { return iter < oth.iter; }
        bool operator<=(const IteratorWrapper& oth) const { return iter <= oth.iter; }
        bool operator>=(const IteratorWrapper& oth) const { return iter >= oth.iter; }
        bool operator>(const IteratorWrapper& oth) const { return iter > oth.iter; }

        IteratorWrapper& operator+=(difference_type n)
        {
            iter += n;
            return *this;
        }
        IteratorWrapper operator+(difference_type n) const
        {
            auto copy = *this;
            return copy += n;
        }
        friend IteratorWrapper operator+(difference_type n, const IteratorWrapper& it)
        {
            return it + n;
        }

        IteratorWrapper& operator-=(difference_type n)
        {
            iter -= n;
            return *this;
        }
        IteratorWrapper operator-(difference_type n) const
        {
            auto copy = *this;
            return copy -= n;
        }
        friend IteratorWrapper operator-(difference_type n, const IteratorWrapper& it)
        {
            return it - n;
        }

        difference_type operator-(const IteratorWrapper& o) const
        {
            return iter - o.iter;
        }

        value_type operator[](difference_type n) const
        {
            return deref(n);
        }

    private:
        IteratorWrapper(BaseIter iter, const BotInfo* owner)
            : iter(std::move(iter)),
            owner(owner),
            cache(owner->source, {})
        {};

        Command deref(difference_type n = 0) const
        {
            return {owner->source, {owner->wrapped, iter[n].get()}};
        }

        BaseIter iter;
        const BotInfo* owner;
        mutable Command cache;
        friend BotInfo;
    };

    using wrapped_container = std::vector<td::td_api::object_ptr<td::td_api::botCommand>>;

public:
    using value_type = Command;
    using reference = const value_type&;
    using pointer = const value_type*;
    using size_type = std::size_t;
    using iterator = IteratorWrapper<wrapped_container::const_iterator>;
    using reverse_iterator = IteratorWrapper<wrapped_container::const_reverse_iterator>;

    using UnfetchableWrapper::UnfetchableWrapper;

    auto description() const { return get_value(&wrapped_type::description_); }
    bool empty() const { return wrapped->commands_.empty(); }
    iterator begin() const { return iterator{wrapped->commands_.begin(), this}; }
    iterator cbegin() const { return begin(); }
    iterator end() const { return iterator{wrapped->commands_.end(), this}; }
    iterator cend() const { return end(); }
    size_type size() const { return wrapped->commands_.size(); }
};

class ChatMember : public UnfetchableWrapper<ChatMember, td::td_api::chatMember>
{
public:
    enum class Status
    {
        Creator = td::td_api::chatMemberStatusCreator::ID,
        Administrator = td::td_api::chatMemberStatusAdministrator::ID,
        Member = td::td_api::chatMemberStatusMember::ID,
        Restricted = td::td_api::chatMemberStatusRestricted::ID,
        Left = td::td_api::chatMemberStatusLeft::ID,
        Banned = td::td_api::chatMemberStatusBanned::ID,
        Unknown = 0,
    };

    using UnfetchableWrapper::UnfetchableWrapper;

    User user() const { return get_value_wrapper<User>(&wrapped_type::user_id_); }
    User inviter() const { return get_value_wrapper<User>(&wrapped_type::inviter_user_id_); }
    auto joined_date() const { return get_value_date(&wrapped_type::joined_chat_date_); }
    bool is_installed_bot() const { return bool(wrapped->bot_info_); }
    BotInfo bot_info() const { return {source, {wrapped, wrapped->bot_info_.get()}}; }
    Status status() const { return get_value_id(&wrapped_type::status_, Status::Unknown); }
};

} // namespace teleglax

#endif // TELEGLAX_OBJECTS_CHAT_MEMBER_HPP
