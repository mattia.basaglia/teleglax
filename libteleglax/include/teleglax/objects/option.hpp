#ifndef TELEGLAX_OBJECTS_OPTION_HPP
#define TELEGLAX_OBJECTS_OPTION_HPP

#include <variant>
#include <ostream>
#include <td/telegram/td_api.h>

namespace teleglax {

class OptionValue
{
public:
    enum class Type
    {
        Empty,
        Boolean,
        Integer,
        String,
    };

    OptionValue() = default;

    OptionValue(const td::td_api::optionValueEmpty&)
    {}

    OptionValue(const td::td_api::optionValueBoolean& v)
        : value(v.value_)
    {}

    OptionValue(const td::td_api::optionValueInteger& v)
        : value(v.value_)
    {}

    OptionValue(const td::td_api::optionValueString& v)
        : value(v.value_)
    {}

    OptionValue(const td::td_api::OptionValue& v)
    {
        using namespace td::td_api;
        switch ( v.get_id() )
        {
            case optionValueBoolean::ID:
                value = static_cast<const optionValueBoolean&>(v).value_;
                break;
            case optionValueInteger::ID:
                value = static_cast<const optionValueInteger&>(v).value_;
                break;
            case optionValueString::ID:
                value = static_cast<const optionValueString&>(v).value_;
                break;
        }
    }

    Type type() const
    {
        return Type(value.index());
    }

    bool as_bool() const
    {
        switch ( type() )
        {
            case Type::Boolean: return std::get<bool>(value);
            case Type::Integer: return std::get<std::int32_t>(value);
            default: return false;
        }
    }

    std::int32_t as_int() const
    {
        switch ( type() )
        {
            case Type::Boolean: return std::get<bool>(value);
            case Type::Integer: return std::get<std::int32_t>(value);
            default: return 0;
        }
    }

    std::string as_string() const
    {
        switch ( type() )
        {
            case Type::Boolean: return std::to_string(std::get<bool>(value));
            case Type::Integer: return std::to_string(std::get<std::int32_t>(value));
            case Type::String: return std::get<std::string>(value);
            default: return {};
        }
    }

    friend std::ostream& operator<<(std::ostream& os, const OptionValue& v)
    {
        switch ( v.type() )
        {
            case Type::Boolean: return os << std::get<bool>(v.value);
            case Type::Integer: return os << std::get<std::int32_t>(v.value);
            case Type::String: return os << std::get<std::string>(v.value);
            default: return os;
        }
    }

private:
    std::variant<std::monostate, bool, std::int32_t, std::string> value;
};

} // namespace teleglax
#endif // TELEGLAX_OBJECTS_OPTION_HPP
