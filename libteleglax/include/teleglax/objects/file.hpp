#ifndef TELEGLAX_OBJECTS_FILE_HPP
#define TELEGLAX_OBJECTS_FILE_HPP

#include "wrapper.hpp"

namespace teleglax {

class File : public BaseWrapper<File, td::td_api::file>
{
private:
    using LocalFile = td::td_api::localFile;
    using RemoteFile = td::td_api::remoteFile;

public:
    using BaseWrapper::BaseWrapper;

    void refresh()
    {
        wrapped = source->request<td::td_api::getFile>(id_).get();
    }

    auto size() const
    {
        auto sz = get_value(&wrapped_type::size_);
        return  sz != 0 ? sz : get_value(&wrapped_type::expected_size_);
    }

    bool size_is_estimate() const
    {
        return get_value(&wrapped_type::size_) == 0;
    }

    auto path() const { return get_value_local(&LocalFile::path_); }
    auto can_be_downloaded() const { return get_value_local(&LocalFile::can_be_downloaded_); }
    auto can_be_deleted() const { return get_value_local(&LocalFile::can_be_deleted_); }
    auto is_downloading_completed() const { return get_value_local(&LocalFile::is_downloading_completed_); }
    auto downloaded_size() const
    {
        return is_downloading_completed() ?
            get_value_local(&LocalFile::downloaded_prefix_size_) :
            get_value_local(&LocalFile::downloaded_size_);
    }
    auto remote_id() const { return get_value_remote(&RemoteFile::id_); }
    auto is_uploading_active() const { return get_value_remote(&RemoteFile::is_uploading_active_); }
    auto is_uploading_completed() const { return get_value_remote(&RemoteFile::is_uploading_completed_); }
    auto uploaded_size() const { return get_value_remote(&RemoteFile::uploaded_size_); }

    std::future<File> download(std::int32_t priority=1) const
    {
        std::promise<File> promise;
        auto future = promise.get_future();
        if ( !can_be_downloaded() || is_downloading_completed() )
        {
            promise.set_value(*this);
        }
        else
        {
            auto wrapped = this->wrapped;
            wrapped->local_->can_be_downloaded_ = false;
            auto source = this->source;
            source->async_request<td::td_api::downloadFile>(
                [promise=std::move(promise), source, wrapped]
                (td::td_api::object_ptr<td::td_api::Object> obj) mutable {
                    if ( obj->get_id() == td::td_api::error::ID )
                    {
                        promise.set_exception(std::make_exception_ptr(ApiError(
                            *td::move_tl_object_as<td::td_api::error>(obj)
                        )));
                        wrapped->local_->can_be_downloaded_ = true;
                        return;
                    }
                    auto file = td::move_tl_object_as<td::td_api::file>(obj);
                    wrapped->local_ = std::move(file->local_);
                    promise.set_value({source, wrapped->id_, std::move(wrapped)});
                },
                id_,
                priority
            );
        }
        return future;
    }

private:
    template<class MemType, class Default=MemType>
        MemType get_value_local(
            MemType (LocalFile::*ptr),
            Default&& default_value = MemType()
        ) const
        {
            if ( !ensure_object() )
                return std::forward<Default>(default_value);

            return detail::get_value(
                ensure_object()->local_,
                ptr,
                std::forward<Default>(default_value)
            );
        }

    template<class MemType, class Default=MemType>
        MemType get_value_remote(
            MemType (RemoteFile::*ptr),
            Default&& default_value = MemType()
        ) const
        {
            if ( !ensure_object() )
                return std::forward<Default>(default_value);

            return detail::get_value(
                ensure_object()->remote_,
                ptr,
                std::forward<Default>(default_value)
            );
        }
};

} // namespace teleglax
#endif // TELEGLAX_OBJECTS_FILE_HPP
