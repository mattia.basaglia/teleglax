#ifndef TELEGLAX_OBJECT_MESSAGE_HPP
#define TELEGLAX_OBJECT_MESSAGE_HPP

#include "user.hpp"
#include "chat.hpp"
#include "file.hpp"
#include "message_content.hpp"


namespace teleglax {

class Message : public BaseWrapper<Message, td::td_api::message>
{
public:
    enum class SendState
    {
        Sent = 0,
        Pending = td::td_api::messageSendingStatePending::ID,
        Failed = td::td_api::messageSendingStateFailed::ID,
    };
    enum Permissions
    {
        None = 0,
        Edit = 0x001,
        Forward = 0x002,
        DeleteForSelf = 0x004,
        DeleteForAll = 0x008,
    };

    using ContentType = MessageContent::Type;

    Message(Teleglax* source, id_type id, pointer wrapped={}, bool suppress_notification=false)
        : BaseWrapper(source, id, std::move(wrapped)),
          suppress_notification_(suppress_notification)
    {}

    Message(Teleglax& source, id_type id, pointer wrapped={}, bool suppress_notification=false)
        : BaseWrapper(source, id, std::move(wrapped)),
          suppress_notification_(suppress_notification)
    {}

    User sender() const { return get_value_wrapper<User>(&wrapped_type::sender_user_id_); };
    Chat chat() const { return get_value_wrapper<Chat>(&wrapped_type::chat_id_); };

    int permissions() const
    {
        int perms = None;
        if ( ensure_object() )
        {
            if ( wrapped->can_be_edited_ )
                perms |= Edit;
            if ( wrapped->can_be_forwarded_ )
                perms |= Forward;
            if ( wrapped->can_be_deleted_only_for_self_ )
                perms |= DeleteForSelf;
            if ( wrapped->can_be_deleted_for_all_users_ )
                perms |= DeleteForAll;
        }
        return perms;
    }
    /// \todo reply_markup_ (wrap content_)

    auto send_state() const { return get_value_id(&wrapped_type::sending_state_, SendState::Sent); }
    auto is_outgoing() const { return get_value(&wrapped_type::is_outgoing_); }
    auto is_channel_post() const { return get_value(&wrapped_type::is_channel_post_); }
    auto contains_unread_mention() const { return get_value(&wrapped_type::contains_unread_mention_); }
    auto date() const { return get_value_date(&wrapped_type::date_); }
    auto edit_date() const { return get_value_date(&wrapped_type::edit_date_); }
    Message reply_to() const  { return get_value_wrapper<Message>(&wrapped_type::reply_to_message_id_); }
    auto ttl() const { return get_value_date(&wrapped_type::ttl_); }
    auto media_album_id() const { return get_value(&wrapped_type::media_album_id_); }
    std::chrono::milliseconds ttl_expires_in() const
    {
        if ( !ensure_object() )
            return {};
        return std::chrono::milliseconds(int(wrapped->ttl_expires_in_ * 1000));
    }
    User via_bot() const { return get_value_wrapper<User>(&wrapped_type::via_bot_user_id_); };
    auto author_signature() const { return get_value(&wrapped_type::author_signature_); }
    auto views() const { return get_value(&wrapped_type::views_); }
    td::td_api::MessageContent* content() const
    {
        if ( !ensure_object() )
            return nullptr;
        return wrapped->content_.get();
    }

    bool is_forwarded() const
    {
        if ( !ensure_object() )
            return false;
        return bool(wrapped->forward_info_);
    }
    Chat forwarded_from() const
    {
        using namespace td::td_api;
        if ( !ensure_object() )
            if ( const auto& fi = wrapped->forward_info_ )
            {
                auto objid = fi->get_id();
                if ( objid == messageForwardedFromUser::ID )
                    return Chat(source, static_cast<messageForwardedFromUser*>(fi.get())->sender_user_id_);
                if ( objid == messageForwardedPost::ID )
                    return Chat(source, static_cast<messageForwardedPost*>(fi.get())->chat_id_);
            }
        return Chat(source, 0);
    }
    std::chrono::system_clock::time_point forwardeddate() const
    {
        using namespace td::td_api;
        if ( !ensure_object() )
            if ( const auto& fi = wrapped->forward_info_ )
            {
                auto objid = fi->get_id();
                if ( objid == messageForwardedFromUser::ID )
                    return detail::get_value_date(
                        detail::pointer_cast<messageForwardedFromUser>(fi),
                        &messageForwardedFromUser::date_
                    );
                if ( objid == messageForwardedPost::ID )
                    return detail::get_value_date(
                        detail::pointer_cast<messageForwardedPost>(fi),
                        &messageForwardedPost::date_
                    );
            }
        return {};
    }
    Chat forwarded_saved_messages_chat() const
    {
        using namespace td::td_api;
        if ( !ensure_object() )
            if ( const auto& fi = wrapped->forward_info_ )
            {
                auto objid = fi->get_id();
                if ( objid == messageForwardedFromUser::ID )
                    return Chat(source, static_cast<messageForwardedFromUser*>(fi.get())->forwarded_from_chat_id_);
                if ( objid == messageForwardedPost::ID )
                    return Chat(source, static_cast<messageForwardedPost*>(fi.get())->forwarded_from_chat_id_);
            }
        return Chat(source, 0);
    }
    Message forwarded_saved_messages_message() const
    {
        using namespace td::td_api;
        if ( !ensure_object() )
            if ( const auto& fi = wrapped->forward_info_ )
            {
                auto objid = fi->get_id();
                if ( objid == messageForwardedFromUser::ID )
                    return Message(source, static_cast<messageForwardedFromUser*>(fi.get())->forwarded_from_message_id_);
                if ( objid == messageForwardedPost::ID )
                    return Message(source, static_cast<messageForwardedPost*>(fi.get())->forwarded_from_message_id_);
            }
        return Message(source, 0);
    }
    Message forwarded_message() const
    {
        using namespace td::td_api;
        if ( !ensure_object() )
            if ( const auto& fi = wrapped->forward_info_; fi && fi->get_id() == messageForwardedPost::ID )
            {
                return Message(source, static_cast<messageForwardedPost*>(fi.get())->message_id_);
            }
        return Message(source, 0);
    }
    std::string forwarded_signature() const
    {
        using namespace td::td_api;
        if ( !ensure_object() )
            if ( const auto& fi = wrapped->forward_info_; fi && fi->get_id() == messageForwardedPost::ID )
            {
                return static_cast<messageForwardedPost*>(fi.get())->author_signature_;
            }
        return {};
    }

    bool suppress_notification() const
    {
        return suppress_notification_;
    }

    void refresh()
    {
        /// \todo
    }

    /// \todo reply_markup_
    std::future<Message> reply(
        MessageContent content,
        bool disable_notification = false,
        bool from_background = false
    ) const
    {
        return chat().send_message(std::move(content), disable_notification, from_background, id_);
    }

    ContentType content_type() const
    {
        return get_value_id(&wrapped_type::content_, ContentType::Unsupported);
    }

    std::string content_text() const
    {
        td::td_api::MessageContent* content = this->content();
        if ( !content )
            return {};

        switch ( content->get_id() )
        {
#define CASE(cls) case td::td_api::cls::ID: \
            return static_cast<td::td_api::cls*>(content)
            CASE(messageText)->text_->text_;
            CASE(messageAnimation)->caption_->text_;
            CASE(messageAudio)->caption_->text_;
            CASE(messageDocument)->caption_->text_;
            CASE(messagePhoto)->caption_->text_;
            CASE(messageSticker)->sticker_->emoji_;
            CASE(messageVideo)->caption_->text_;
            CASE(messageVoiceNote)->caption_->text_;
            CASE(messageVenue)->venue_->title_;
            CASE(messageGame)->game_->title_;
            CASE(messageBasicGroupChatCreate)->title_;
            CASE(messageSupergroupChatCreate)->title_;
            CASE(messageChatChangeTitle)->title_;
            CASE(messageChatUpgradeFrom)->title_;
            CASE(messageCustomServiceAction)->text_;
            CASE(messageWebsiteConnected)->domain_name_;
#undef CASE
        }
        return {};
    }

    File content_media_file()
    {
        td::td_api::MessageContent* content = this->content();

        if ( content )
        {
            td::td_api::object_ptr<td::td_api::file> fptr;
            switch ( content->get_id() )
            {
#define CASEx(cls,att) case td::td_api::cls::ID: \
            fptr = std::move(static_cast<td::td_api::cls*>(content)->att); break
#define CASE(cls,att) CASEx(cls,att->att)
                CASE(messageAnimation, animation_);
                CASE(messageAudio, audio_);
                CASE(messageDocument, document_);
                CASEx(messagePhoto, photo_->sizes_[0]->photo_);
                CASE(messageSticker, sticker_);
                CASE(messageVideo, video_);
                CASEx(messageVideoNote, video_note_->video_);
                CASEx(messageVoiceNote, voice_note_->voice_);
                CASEx(messageGame, game_->photo_->sizes_[0]->photo_);
                CASEx(messageInvoice, photo_->sizes_[0]->photo_);
                CASEx(messageChatChangePhoto, photo_->sizes_[0]->photo_);
#undef CASE
#undef CASEx
            }
            if ( fptr )
            {
                auto id = fptr->id_;
                return File(source, id, {wrapped, fptr.get()});
            }
        }

        return File(source, 0);
    }

    template<class T>
        T* content_as() const
        {
            if ( content()->get_id() != T::ID )
                return nullptr;
            return static_cast<T*>(wrapped->content_.get());
        }

private:
    bool suppress_notification_ = false;
};

} // namespace teleglax
#endif // TELEGLAX_OBJECT_MESSAGE_HPP
