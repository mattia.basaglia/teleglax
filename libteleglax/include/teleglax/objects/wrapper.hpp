#ifndef TELEGLAX_OBJECTS_WRAPPER
#define TELEGLAX_OBJECTS_WRAPPER

#include <chrono>

#include "../teleglax.hpp"

namespace teleglax {
namespace detail {
    template<class T>
    class dummy_ptr
    {
    public:
        dummy_ptr(T* ptr=nullptr) noexcept : ptr(ptr) {}

        constexpr T* get() const noexcept
        {
            return ptr;
        }

        constexpr explicit operator bool() const noexcept
        {
            return ptr;
        }

        constexpr T& operator*() const noexcept
        {
            return *ptr;
        }

        constexpr T* operator->() const noexcept
        {
            return ptr;
        }

        T* ptr;
    };

    template<class T, class F, template<class> class W = dummy_ptr>
    dummy_ptr<T> pointer_cast(const W<F>& ptr)
    {
        return static_cast<T*>(ptr.get());
    }

    template<class ConvTo, class Object>
    ConvTo get_id(const td::td_api::object_ptr<Object>& obj, ConvTo default_v)
    {
        return obj ? ConvTo(obj->get_id()) : default_v;
    }

    template<class Class, class MemType, class Default, template<class> class Pointer=td::td_api::object_ptr>
    MemType get_value(const Pointer<Class>& owner, MemType (Class::*member), Default&& default_value)
    {
        return owner ? (owner.get()->*member) : std::forward<Default>(default_value);
    }

    template<class Class, class MemType, class ConvTo, template<class> class Pointer=td::td_api::object_ptr>
    ConvTo get_value_id(const Pointer<Class>& owner, MemType (Class::*member), ConvTo default_value)
    {
        return owner ? get_id<ConvTo>(owner.get()->*member, default_value) : default_value;
    }

    template<class Class, class MemType, template<class> class Pointer=td::td_api::object_ptr>
    std::chrono::system_clock::time_point get_value_date(const Pointer<Class>& owner, MemType (Class::*member))
    {
        using clock = std::chrono::system_clock;
        return owner ? clock::from_time_t(std::time_t(owner.get()->*member)) : clock::time_point();
    }

    template<class IntType>
    std::chrono::system_clock::time_point convert_date(IntType timestamp)
    {
        using clock = std::chrono::system_clock;
        return clock::from_time_t(std::time_t(timestamp));
    }

} // namespace detail


template<class Derived, class Wrapped>
class UnfetchableWrapper
{
public:
    using wrapped_type = Wrapped;
    using pointer = std::shared_ptr<wrapped_type>;

    static Derived from_td_pointer(Teleglax* source, td::td_api::object_ptr<Wrapped> w)
    {
        auto id = w->id_;
        return Derived(source, std::move(w));
    }

    UnfetchableWrapper(Teleglax* source, pointer wrapped)
        : source(source), wrapped(std::move(wrapped))
    {}

    UnfetchableWrapper(Teleglax& source, pointer wrapped)
        : source(&source), wrapped(std::move(wrapped))
    {}

    Teleglax& client() const
    {
        return *source;
    }

    const pointer& td_api_object() const
    {
        return wrapped;
    }

    bool is_null() const
    {
        return !bool(wrapped);
    }

    explicit operator bool() const
    {
        return !is_null();
    }

protected:
    template<class MemType, class Default=MemType>
        MemType get_value(MemType (wrapped_type::*ptr), Default&& default_value = MemType()) const
        {
            return detail::get_value(wrapped, ptr, std::forward<Default>(default_value));
        }

    template<class MemType, class Default>
        Default get_value_id(MemType (wrapped_type::*ptr), Default default_value) const
        {
            return detail::get_value_id(wrapped, ptr, default_value);
        }

    template<class MemType>
        auto get_value_date(MemType (wrapped_type::*ptr)) const
        {
            return detail::get_value_date(wrapped, ptr);
        }

    template<class Wrapper, class MemType>
        Wrapper get_value_wrapper(MemType (wrapped_type::*ptr)) const
        {
            return Wrapper(source, wrapped ? wrapped.get()->*ptr : 0);
        }

    Teleglax* source;
    pointer wrapped;
};

template<class Derived, class Wrapped, class IdType=std::decay_t<decltype(std::declval<Wrapped>().id_)>>
class BaseWrapper
{
public:
    using id_type = IdType;
    using wrapped_type = Wrapped;
    using pointer = std::shared_ptr<wrapped_type>;

    static Derived from_td_pointer(Teleglax* source, td::td_api::object_ptr<Wrapped> w)
    {
        auto id = w->id_;
        return Derived(source, id, std::move(w));
    }

    BaseWrapper(Teleglax* source, id_type id, pointer wrapped={})
        : source(source), id_(id), wrapped(std::move(wrapped))
    {}

    BaseWrapper(Teleglax& source, id_type id, pointer wrapped={})
        : source(&source), id_(id), wrapped(std::move(wrapped))
    {}

    id_type id() const { return id_; }

    bool operator==(const BaseWrapper& other) const
    {
        return id_ == other.id_ && source == other.source;
    }

    bool operator!=(const BaseWrapper& other) const
    {
        return !(*this == other);
    }

    bool is_null() const
    {
        return id_ == 0;
    }

    explicit operator bool() const
    {
        return !is_null();
    }

    Teleglax& client() const
    {
        return *source;
    }

    const pointer& td_api_object() const
    {
        return wrapped;
    }

protected:
    template<class MemType, class Default=MemType>
        MemType get_value(MemType (wrapped_type::*ptr), Default&& default_value = MemType()) const
        {
            return detail::get_value(ensure_object(), ptr, std::forward<Default>(default_value));
        }

    template<class MemType, class Default>
        Default get_value_id(MemType (wrapped_type::*ptr), Default default_value) const
        {
            return detail::get_value_id(ensure_object(), ptr, default_value);
        }

    template<class MemType>
        auto get_value_date(MemType (wrapped_type::*ptr)) const
        {
            return detail::get_value_date(ensure_object(), ptr);
        }

    template<class Wrapper, class MemType>
        Wrapper get_value_wrapper(MemType (wrapped_type::*ptr)) const
        {
            ensure_object();
            return Wrapper(source, wrapped ? wrapped.get()->*ptr : 0);
        }

    const pointer& ensure_object() const
    {
        if ( id_ && !wrapped )
            static_cast<Derived*>(const_cast<BaseWrapper*>(this))->refresh();
        return wrapped;
    }

    Teleglax* source;
    id_type id_;
    mutable pointer wrapped;
};


} // namespace teleglax
#endif // TELEGLAX_OBJECTS_WRAPPER
