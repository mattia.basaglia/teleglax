#ifndef TELEGLAX_EVENT_TRIGGER_HPP
#define TELEGLAX_EVENT_TRIGGER_HPP

#include <td/telegram/td_api.h>

namespace teleglax {

class Message;
class Chat;

class EventTrigger
{
public:
    enum class ConnectionStatus
    {
        WaitingForNetwork = td::td_api::connectionStateWaitingForNetwork::ID,
        ConnectingToProxy = td::td_api::connectionStateConnectingToProxy::ID,
        Connecting = td::td_api::connectionStateConnecting::ID,
        Updating = td::td_api::connectionStateUpdating::ID,
        Ready = td::td_api::connectionStateReady::ID,
    };


    virtual ~EventTrigger(){}
    virtual void on_connection_status(ConnectionStatus status) {}
    virtual void on_new_message(const Message& msg, bool disable_notifications){}
    virtual void on_new_chat(const Chat& chat){}
    virtual void on_chat_changed(const Chat& chat){}
};

} // namespace teleglax
#endif // TELEGLAX_EVENT_TRIGGER_HPP
