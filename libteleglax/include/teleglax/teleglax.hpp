#ifndef TELEGLAX_HPP
#define TELEGLAX_HPP

#include <memory>
#include <map>
#include <functional>
#include <mutex>
#include <future>

#include <td/telegram/Client.h>
#include <td/telegram/td_api.h>
#include <td/telegram/td_api.hpp>

#include "userdb.hpp"
#include "callback.hpp"
#include "authorization_source.hpp"
#include "objects/app_info.hpp"
#include "objects/option.hpp"
#include "objects/input_file.hpp"
#include "fetcher_iterator.hpp"
#include "event_trigger.hpp"

namespace teleglax {

class User;
class Chat;


namespace detail {

class ChatFetcher : public Fetcher<ChatFetcher, Chat, td::td_api::chats>
{
public:
    bool receive(td::td_api::object_ptr<td::td_api::chats> llchats);
    void prepare_result(){}
    void send_request();
    void on_advance(const std::vector<Chat>& result);

    std::int64_t offset_order;
    std::int64_t offset_chat_id;
};

} // namespace detail

class Teleglax
{
public:
    enum class AuthStatus
    {
        Unauthed = 0,
        Authed = 1,
        Error = 2,
        Finished = Authed,
    };

    explicit Teleglax(AppInfo app_info, AuthSource auth, std::unique_ptr<EventTrigger> event)
        : client(std::make_unique<td::Client>()),
          app_info(std::move(app_info)),
          auth(std::move(auth)),
          event(std::move(event))
    {}
    Teleglax(Teleglax&&) = default;

    bool connect()
    {
        while ( auth_status < AuthStatus::Finished )
        {
            if ( !poll(10) )
                return false;
        }
        return auth_status == AuthStatus::Authed;
    }

    /**
     * \brief Builds and sends a request, to be handled by a callback
     */
    void send_request(td::td_api::object_ptr<td::td_api::Function> f, TeleglaxCallback handler)
    {
        auto request_id = next_request_id();
        {
            std::unique_lock<MovableMutex> lock{handlers_mutex};
            handlers.emplace(request_id, std::move(handler));
        }
        client->send({request_id, std::move(f)});
    }

    /**
     * \brief Builds and sends a request, to be handled by a callback
     */
    template<class tdlibT, class... Args>
        void async_request(TeleglaxCallback handler, Args&&... args)
        {
            send_request(
                td::td_api::make_object<tdlibT>(std::forward<Args>(args)...),
                std::move(handler)
            );
        }

    template<class tdlibT>
        std::future<typename tdlibT::ReturnType> request(td::td_api::object_ptr<tdlibT> arg)
        {
            std::promise<typename tdlibT::ReturnType> promise;
            auto future = promise.get_future();
            send_request(
                std::move(arg),
                [prom=std::move(promise)](td::td_api::object_ptr<td::td_api::Object> obj) mutable {
                    if ( obj->get_id() == td::td_api::error::ID )
                    {
                        prom.set_exception(std::make_exception_ptr(ApiError(
                            *td::move_tl_object_as<td::td_api::error>(obj)
                        )));
                        return;
                    }
                    prom.set_value(td::move_tl_object_as<typename tdlibT::ReturnType::element_type>(obj));
                }
            );
            return std::move(future);
        }

    /**
     * \brief Builds and sends a request
     * \return A future which will be populated with the returned value or an
     *         exception if the API returns an error code
     */
    template<class tdlibT, class... Args>
        std::future<typename tdlibT::ReturnType> request(Args&&... args)
        {
            return request<tdlibT>(td::td_api::make_object<tdlibT>(std::forward<Args>(args)...));
        }
    /**
     * \brief Builds and sends a request
     * \return A future without state or an exception if the API returns an error code
     */
    template<class tdlibT, class... Args>
        std::future<void> request_void(Args&&... args)
        {
            std::promise<void> promise;
            auto future = promise.get_future();
            send_request(
                td::td_api::make_object<tdlibT>(std::forward<Args>(args)...),
                [prom=std::move(promise)](td::td_api::object_ptr<td::td_api::Object> obj) mutable {
                    if ( obj->get_id() == td::td_api::error::ID )
                    {
                        prom.set_exception(std::make_exception_ptr(ApiError(
                            *td::move_tl_object_as<td::td_api::error>(obj)
                        )));
                        return;
                    }
                    prom.set_value();
                }
            );
            return future;
        }

    /**
     * \brief Sends a request to be handled by the default internal handlers
     */
    template<class tdlibT, class... Args>
        void internal_request(Args&&... args)
        {
            async_request<tdlibT>(
                [this](td::td_api::Object& object) {
                    process_update(object);
                },
                std::forward<Args>(args)...
            );
        }

    /**
     * \brief Poll for a response
     * \param timeout max number of seconds
     * \return \b false on error or timeout
     */
    bool poll(double timeout)
    {
        td::Client::Response response = client->receive(timeout);
        if ( !response.object )
            return false;

        if ( response.id == 0 )
        {
            process_update(*response.object);
            return true;
        }

        std::unique_lock<MovableMutex> lock{handlers_mutex};
        if ( auto it = handlers.find(response.id); it != handlers.end() )
        {
            it->second(std::move(response.object));
            handlers.erase(it);
            return true;
        }
        throw UnknownObjectError(
            *response.object,
            "response to request " + std::to_string(response.id)
        );
    }

    const AppInfo& info() const
    {
        return app_info;
    }

    void set_info(const AppInfo& info)
    {
        app_info = info;
    }

    const UserDb& users() const { return user_db; }
    const ChatDb& chats() const { return chat_db; }
    const std::map<std::string, OptionValue>& options() const { return tg_options; }
    AuthSource& auth_source() { return auth; }
    EventTrigger* event_trigger() { return event.get(); }

    OptionValue option(const std::string& name, OptionValue def = {}) const
    {
        auto it = tg_options.find(name);
        if ( it == tg_options.end() )
            return it->second;
        return def;
    }


    template<class Callable>
    void register_file_generation(const std::string& generation, Callable&& callback)
    {
        file_generation.register_callback(generation, std::forward<Callable>(callback));
    }

    // request wrappers
    std::future<std::vector<Chat>> get_chats(std::int32_t count, const Chat& from);
    std::future<std::vector<Chat>> get_chats(std::int32_t count);
    FetchRange<detail::ChatFetcher> get_chats_range();
    std::future<void> logout()  { return request_void<td::td_api::logOut>(); }
    std::future<User> me();
    /**
     * \brief Sets the name for the current user
     */
    std::future<void> set_name(const std::string& first, const std::string& last = {})
    {
        return request_void<td::td_api::setName>(first, last);
    }
    std::future<void> set_bio(const std::string& bio)
    {
        return request_void<td::td_api::setBio>(bio);
    }

    /**
     * \brief Fetches a user
     * \note This will always query tdblib; for a cached value use users(),
     *       to optionally use a cached value or a new query use User()
     */
    std::future<User> get_user(std::int32_t id);

private:
    template<class T>
    void on_auth_state(T& state)
    {
        auth_status = AuthStatus::Error;
        throw UnknownObjectError(state, "auth state");
    }

    void on_update(td::td_api::updateAuthorizationState& update_authorization_state)
    {
        td::td_api::downcast_call(
            *update_authorization_state.authorization_state_,
            [this](auto& state){
                on_auth_state(state);
            }
        );
    }

    template<class T>
    void on_update(T& update)
    {
        throw UnknownObjectError(update, "update");
    }

    void process_update(td::td_api::Object& update)
    {
        td::td_api::downcast_call(update, [this](auto & dcupdate) {
            on_update(dcupdate);
        });
    }

    void on_update(td::td_api::error& error)
    {
        if ( error.code_ == 406 )
            return; // API says to ignore this
        throw ApiError(error);
    }

    static std::uint64_t next_request_id() noexcept
    {
        static std::uint64_t i = 0;
        return ++i;
    }

    struct MovableMutex : public std::recursive_mutex
    {
        using std::recursive_mutex::recursive_mutex;
        MovableMutex(MovableMutex&&) noexcept {}
    };

    std::unique_ptr<td::Client> client;
    std::map<std::uint64_t, TeleglaxCallback> handlers;
    MovableMutex handlers_mutex;
    UserDb user_db;
    ChatDb chat_db;
    std::map<std::string, OptionValue> tg_options;
    AuthStatus auth_status = AuthStatus::Unauthed;
    AppInfo app_info;
    AuthSource auth;
    CustomFileGeneration file_generation;
    std::unique_ptr<EventTrigger> event;

};

} // namespace teleglax
#endif // TELEGLAX_HPP
