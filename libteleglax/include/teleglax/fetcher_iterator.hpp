#ifndef TELEGLAX_FETCHER_ITERATOR_HPP
#define TELEGLAX_FETCHER_ITERATOR_HPP

#include <vector>
#include <future>
#include "exceptions.hpp"
#include "tdlib_helpers.hpp"

namespace teleglax {

class Teleglax;

template<class Derived, class Value, class ApiResponse, class Container=std::vector<Value>>
class Fetcher
{
public:
    using value_type = Value;
    using container_type = Container;
    static constexpr std::int32_t COUNT_DEFAULT = -1;
    static constexpr std::int32_t COUNT_FETCH_ALL = -10;

    Fetcher(Teleglax* source, std::int32_t count = 0)
        : source(source), count(count)
    {}

    void operator()(td::td_api::object_ptr<td::td_api::Object> response)
    {
        if ( td_is_compatible<ApiResponse>(response) )
        {
            if ( !derived().receive(td::td_api::move_object_as<ApiResponse>(response)) )
                on_end();
            else
                state_check();
        }
        else if ( td_is_compatible<td::td_api::error>(response) )
        {
            promise.set_exception(std::make_exception_ptr(ApiError(
                *td::td_api::move_object_as<td::td_api::error>(response)
            )));
        }
        else
        {
            promise.set_exception(std::make_exception_ptr(
                UnknownObjectError(*response, "fetcher callback argument")
            ));
        }
    }

    std::future<Container> start(std::int32_t count = COUNT_DEFAULT)
    {
        if ( count != COUNT_DEFAULT)
            this->count = count;
        promise = std::promise<Container>{};
        temp_objects = Container{};
        auto future = promise.get_future();
        state_check();
        return future;
    }

    void advance(const Container& result)
    {
        if ( !result.empty() )
            derived().on_advance(result);
    }

protected:
    void on_end()
    {
        derived().prepare_result();
        promise.set_value(std::move(temp_objects));
    }

    Derived& derived()
    {
        return static_cast<Derived&>(*this);
    }

    void state_check()
    {
        if ( count <= 0 && count != COUNT_FETCH_ALL )
            return on_end();
        static_cast<Derived*>(this)->send_request();
    }

    void decrease_count(std::int32_t amount)
    {
        if ( count != COUNT_FETCH_ALL )
            count -= amount;
    }

    std::int32_t positive_count() const
    {
        return count >= 0 ? count : -count;
    }

    Teleglax* source;
    std::int32_t count;

    Container temp_objects;
    std::promise<Container> promise;
};

template<class Fetchclass>
class FetchRange
{
public:
    using value_type = typename Fetchclass::value_type;
    using container_type = typename Fetchclass::container_type;

    class iterator
    {
    public:
    using value_type = typename Fetchclass::value_type;

        bool operator==(const iterator& oth) const
        {
            if ( finished() && oth.finished() )
                return true;
            return oth.range == range;
        }

        bool operator!=(const iterator& oth) const
        {
            return !(*this == oth);
        }

        bool finished() const
        {
            return !range || range->finished;
        }

        iterator& operator++()
        {
            range->next();
            return *this;
        }

        value_type& operator*() const
        {
            return range->get();
        }

        value_type* operator->() const
        {
            return range->get();
        }

    private:
        iterator(FetchRange* range) : range(range) {}
        FetchRange* range;
        friend FetchRange;
    };

    FetchRange(Fetchclass fetcher)
        : fetcher(std::move(fetcher))
    {
        fetch();
    }

    iterator end() const { return {nullptr}; }
    iterator begin(){ return {this}; }

    value_type& get()
    {
        return batch[batch_offset];
    }

    void next()
    {
        ++batch_offset;
        if ( batch_offset >= batch.size() )
        {
            if ( std::int32_t(batch_offset) < batch_size )
                finished = true;
            else
                fetch();
        }
    }

private:
    void fetch()
    {
        batch_offset = 0;
        fetcher.advance(batch);
        batch = fetcher.start(batch_size).get();

        if ( batch.empty() )
            finished = true;
    }

    Fetchclass fetcher;
    std::int32_t batch_size = 10;
    container_type batch;
    bool finished = false;
    std::size_t batch_offset = 0;
};

} // namespace teleglax
#endif // TELEGLAX_FETCHER_ITERATOR_HPP
