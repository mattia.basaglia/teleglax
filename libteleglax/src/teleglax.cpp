#include "teleglax/teleglax.hpp"

#include "teleglax/objects/chat.hpp"
#include "teleglax/objects/user.hpp"

namespace teleglax {

bool detail::ChatFetcher::receive(td::td_api::object_ptr<td::td_api::chats> llchats)
{
    if ( llchats->chat_ids_.empty() )
        return false;

    decrease_count(llchats->chat_ids_.size());
    for ( auto chat_id : llchats->chat_ids_ )
    {
        temp_objects.push_back(Chat(source, chat_id));
    }

    offset_chat_id = temp_objects.back().id();
    offset_order = temp_objects.back().order();

    return true;
}

void detail::ChatFetcher::send_request()
{
    source->async_request<td::td_api::getChats>(
        std::move(*this),
        offset_order,
        offset_chat_id,
        positive_count()
    );
}

void detail::ChatFetcher::on_advance(const std::vector<Chat>& result)
{
    offset_chat_id = result.back().id();
    offset_order = result.back().order();
}

std::future<std::vector<Chat>> Teleglax::get_chats(std::int32_t count, const Chat& from)
{
    return detail::ChatFetcher{{this}, from.order(), from.id()}.start(count);
}

std::future<std::vector<Chat>> Teleglax::get_chats(std::int32_t count)
{
    return get_chats(count, Chat{this, 0});
}

FetchRange<detail::ChatFetcher> Teleglax::get_chats_range()
{
    Chat dummy{this, 0};
    return {detail::ChatFetcher{{this}, dummy.order(), dummy.id()}};
}

std::future<User> Teleglax::me()
{
    std::promise<User> promise;
    auto res = promise.get_future();
    async_request<td::td_api::getMe>(
        [promise=std::move(promise), this]
        (td::td_api::object_ptr<td::td_api::Object> obj) mutable {
        if ( obj->get_id() == td::td_api::error::ID )
        {
            promise.set_exception(std::make_exception_ptr(ApiError(
                *td::move_tl_object_as<td::td_api::error>(obj)
            )));
            return;
        }
        auto result = td::td_api::move_object_as<td::td_api::user>(obj);
        auto id = result->id_;
        promise.set_value(User(this, id, std::move(result)));
    });
    return res;
}

std::future<User> Teleglax::get_user(std::int32_t id)
{
    std::promise<User> promise;
    auto res = promise.get_future();
    auto lambda = [promise=std::move(promise), this]
        (td::td_api::object_ptr<td::td_api::Object> obj) mutable {
        if ( obj->get_id() == td::td_api::error::ID )
        {
            promise.set_exception(std::make_exception_ptr(ApiError(
                *td::move_tl_object_as<td::td_api::error>(obj)
            )));
            return;
        }
        auto result = td::td_api::move_object_as<td::td_api::user>(obj);
        auto id = result->id_;
        promise.set_value(User(this, id, std::move(result)));
    };
    async_request<td::td_api::getUser>(std::move(lambda), id);
    return res;
}

} // namespace teleglax
