#include "teleglax/objects/chat.hpp"

#include <algorithm>

#include "teleglax/objects/message.hpp"
#include "teleglax/objects/chat_member.hpp"

namespace teleglax {

template<class Derived>
bool detail::GetMessagesCallback<Derived>
    ::receive(td::td_api::object_ptr<td::td_api::messages> rmessages)
{
    auto llmessages = std::move(rmessages->messages_);
    if ( llmessages.empty() )
        return false;

    for ( auto& msg : llmessages )
    {
        auto id  = msg->id_;
        this->temp_objects.emplace_back(this->source, id, std::move(msg));
        if ( offset < 0 and after )
        {
            if ( id == from_message_id && int(this->temp_objects.size()) < after )
            {
                this->decrease_count(after - this->temp_objects.size());
                after = 0;
            }
        }
    }
    from_message_id = this->temp_objects.back().id();

    offset = 0;
    this->decrease_count(llmessages.size());
    return true;
}

template<class Derived>
void detail::GetMessagesCallback<Derived>::prepare_result()
{
    if ( chronological )
        std::reverse(this->temp_objects.begin(), this->temp_objects.end());
}

template void detail::on_advance<detail::GetMessages, Message>(detail::GetMessages* obj, const std::vector<Message>& result);
template void detail::on_advance<detail::SearchMessages, Message>(detail::SearchMessages* obj, const std::vector<Message>& result);

void detail::GetMessages::send_request()
{
    source->async_request<td::td_api::getChatHistory>(
        std::move(*this),
        chat_id,
        from_message_id,
        offset,
        positive_count(),
        local_only
    );
}


void detail::SearchMessages::send_request()
{
    auto copy_query = query;
    source->async_request<td::td_api::searchChatMessages>(
        std::move(*this),
        chat_id,
        copy_query,
        sender_user_id,
        from_message_id,
        offset,
        positive_count(),
        make_filter()
    );
}

td::td_api::object_ptr<td::td_api::SearchMessagesFilter> detail::SearchMessages::make_filter() const
{
    switch ( filter )
    {
        default:
        case Chat::SearchFilter::Empty:
            return td::td_api::make_object<td::td_api::searchMessagesFilterEmpty>();
        case Chat::SearchFilter::Animation:
            return td::td_api::make_object<td::td_api::searchMessagesFilterAnimation>();
        case Chat::SearchFilter::Audio:
            return td::td_api::make_object<td::td_api::searchMessagesFilterAudio>();
        case Chat::SearchFilter::Document:
            return td::td_api::make_object<td::td_api::searchMessagesFilterDocument>();
        case Chat::SearchFilter::Photo:
            return td::td_api::make_object<td::td_api::searchMessagesFilterPhoto>();
        case Chat::SearchFilter::Video:
            return td::td_api::make_object<td::td_api::searchMessagesFilterVideo>();
        case Chat::SearchFilter::VoiceNote:
            return td::td_api::make_object<td::td_api::searchMessagesFilterVoiceNote>();
        case Chat::SearchFilter::PhotoAndVideo:
            return td::td_api::make_object<td::td_api::searchMessagesFilterPhotoAndVideo>();
        case Chat::SearchFilter::Url:
            return td::td_api::make_object<td::td_api::searchMessagesFilterUrl>();
        case Chat::SearchFilter::ChatPhoto:
            return td::td_api::make_object<td::td_api::searchMessagesFilterChatPhoto>();
        case Chat::SearchFilter::Call:
            return td::td_api::make_object<td::td_api::searchMessagesFilterCall>();
        case Chat::SearchFilter::MissedCall:
            return td::td_api::make_object<td::td_api::searchMessagesFilterMissedCall>();
        case Chat::SearchFilter::VideoNote:
            return td::td_api::make_object<td::td_api::searchMessagesFilterVideoNote>();
        case Chat::SearchFilter::VoiceAndVideoNote:
            return td::td_api::make_object<td::td_api::searchMessagesFilterVoiceAndVideoNote>();
        case Chat::SearchFilter::Mention:
            return td::td_api::make_object<td::td_api::searchMessagesFilterMention>();
        case Chat::SearchFilter::UnreadMention:
            return td::td_api::make_object<td::td_api::searchMessagesFilterUnreadMention>();
    }
}

std::future<std::vector<Message>> Chat::get_history(
    std::int32_t before,
    std::int32_t after,
    std::int64_t from_message_id,
    bool local_only,
    bool chronological
) const
{
    std::int32_t offset = -after;
    std::int32_t count = before + after;
    if ( count < after )
        count = after + 1;

    if ( from_message_id == 0 )
    {
        offset = 0;
        count = before;
    }

    detail::GetMessages callback{{{source}, id_, after, offset, from_message_id, chronological}, local_only};
    return callback.start(count);
}

std::future<Message> Chat::pinned_message() const
{
    std::promise<Message> promise;
    auto future = promise.get_future();
    auto source = this->source;
    source->async_request<td::td_api::getChatPinnedMessage>(
        [promise=std::move(promise), source](td::td_api::object_ptr<td::td_api::Object> obj) mutable {
            if ( obj->get_id() == td::td_api::message::ID )
            {
                auto message = td::td_api::move_object_as<td::td_api::message>(obj);
                auto id = message ? message->id_ : 0;
                promise.set_value(Message(source, id, std::move(message)));
            }
            else if ( obj->get_id() == td::td_api::error::ID )
            {
                auto error = td::td_api::move_object_as<td::td_api::error>(obj);
                if ( error->code_ == 404 )
                    promise.set_value(Message(source, 0));
                else
                    promise.set_exception(std::make_exception_ptr(ApiError(*error)));
            }
            else
            {
                promise.set_exception(std::make_exception_ptr(UnknownObjectError(*obj, "message")));
            }
        },
        id_
    );
    return future;
}

Message Chat::last_read_inbox() const
{
    return get_value_wrapper<Message>(&wrapped_type::last_read_inbox_message_id_);
}

Message Chat::last_read_outbox() const
{
    return get_value_wrapper<Message>(&wrapped_type::last_read_outbox_message_id_);
}

Message Chat::reply_markup_message() const
{
    return get_value_wrapper<Message>(&wrapped_type::reply_markup_message_id_);
}

Message Chat::last_message() const
{
    if ( !ensure_object() || !wrapped->last_message_ )
        return Message{source, 0};
    return Message{source, ensure_object()->last_message_->id_, {wrapped, ensure_object()->last_message_.get()}};
}


std::future<std::vector<Message>> Chat::search(
    std::string query,
    std::int32_t before,
    std::int32_t after,
    std::int64_t from_message_id,
    std::int32_t sender_user_id,
    SearchFilter filter,
    bool chronological
) const
{
    std::int32_t offset = -after;
    std::int32_t count = before + after;
    if ( count < after )
        count = after + 1;

    if ( from_message_id == 0 )
    {
        offset = 0;
        count = before;
    }

    detail::SearchMessages callback{{{source}, id_, after, offset, from_message_id, chronological}, query, sender_user_id, filter};
    return callback.start(count);
}

FetchRange<detail::GetMessages> Chat::history_range(
    std::int64_t from_message_id,
    bool local_only
) const
{
    return {detail::GetMessages{{{source}, id_, 0, 0, from_message_id, false}, local_only}};
}

FetchRange<detail::SearchMessages> Chat::search_range(
    std::string query,
    std::int64_t from_message_id,
    std::int32_t sender_user_id,
    SearchFilter filter
) const
{
    return {detail::SearchMessages{{{source}, id_, 0, 0, from_message_id, false}, query, sender_user_id, filter}};
}

User Chat::user() const
{
    if ( auto u = user_details() )
        return User(source, u->id_, {details, u});
    return User(source, 0);
}

std::future<Message> Chat::send_message(
    MessageContent content,
    bool disable_notification,
    bool from_background,
    std::int64_t reply_to_message_id
) const
{
    if ( !content )
        return {};
    std::promise<Message> promise;
    auto future = promise.get_future();
    source->async_request<td::td_api::sendMessage>(
        [src=source, promise=std::move(promise)]
        (td::td_api::object_ptr<td::td_api::Object> obj) mutable {
            if ( obj->get_id() == td::td_api::error::ID )
            {
                promise.set_exception(std::make_exception_ptr(ApiError(
                    *td::move_tl_object_as<td::td_api::error>(obj)
                )));
                return;
            }
            auto msg = td::move_tl_object_as<td::td_api::message>(obj);
            auto id = msg->id_;
            promise.set_value(Message(src,  id, std::move(msg)));
        },
        id_,
        reply_to_message_id,
        disable_notification,
        from_background,
        td::td_api::object_ptr<td::td_api::ReplyMarkup>{},
        content.to_td_input()
    );
    return future;
}


class ChatMemberFetcher : public Fetcher<ChatMemberFetcher, ChatMember, td::td_api::chatMembers>
{
public:
    bool receive(td::td_api::object_ptr<td::td_api::chatMembers> llcm)
    {
        if ( llcm->members_.empty() )
            return false;

        decrease_count(llcm->members_.size());

        for ( auto&& chatmember : llcm->members_ )
        {
            temp_objects.push_back(ChatMember(source, std::move(chatmember)));
        }

        offset += temp_objects.size();

        return true;
    }

    void prepare_result(){}

    void send_request()
    {
        source->async_request<td::td_api::getSupergroupMembers>(
            std::move(*this),
            supergroup_id,
            make_filter(),
            offset,
            positive_count()
        );
    }

    void on_advance(const std::vector<ChatMember>& result)
    {
    }

    td::td_api::object_ptr<td::td_api::SupergroupMembersFilter> make_filter() const
    {
        return {};
    }

    std::shared_ptr<td::td_api::chat> owner;
    std::int32_t supergroup_id;
    std::int32_t offset = 0;
};

std::future<std::vector<ChatMember>> Chat::chat_members() const
{
    switch ( type() )
    {
        case Type::BasicGroup:
        {
            std::promise<std::vector<ChatMember>> p;
            std::vector<ChatMember> v;
            for ( const auto& cm : basic_group_full_info()->members_ )
                v.push_back({source, {wrapped, cm.get()}});
            p.set_value(std::move(v));
            return p.get_future();
        }
        case Type::Supergroup:
            if ( can_get_members() )
            {
                ChatMemberFetcher callback{{source}, wrapped, related_id()};
                return callback.start(ChatMemberFetcher::COUNT_FETCH_ALL);
            }
        case Type::Private:
        case Type::Secret:
        case Type::Unknown:
        default:
        {
            std::promise<std::vector<ChatMember>> p;
            p.set_value({});
            return p.get_future();
        }
    };
}

} // namespace teleglax
