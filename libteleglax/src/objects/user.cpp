#include "teleglax/objects/user.hpp"
#include "teleglax/objects/chat.hpp"

namespace teleglax {


class GroupsInCommonFetcher : public Fetcher<GroupsInCommonFetcher, Chat, td::td_api::chats>
{
public:
    bool receive(td::td_api::object_ptr<td::td_api::chats> llcm)
    {
        if ( llcm->chat_ids_.empty() )
            return false;

        decrease_count(llcm->chat_ids_.size());

        for ( const auto& chat_id: llcm->chat_ids_ )
        {
            temp_objects.push_back(Chat(source, chat_id));
        }

        offset_chat_id = llcm->chat_ids_.back();

        return true;
    }

    void prepare_result(){}

    void send_request()
    {
        source->async_request<td::td_api::getGroupsInCommon>(
            std::move(*this),
            user_id,
            offset_chat_id,
            positive_count()
        );
    }

    void on_advance(const std::vector<Chat>&)
    {
    }

    std::int32_t user_id;
    std::int64_t offset_chat_id = 0;
};


std::future<std::vector<Chat>> User::groups_in_common() const
{
    ensure_object();
    GroupsInCommonFetcher callback{{source}, id()};
    return callback.start(GroupsInCommonFetcher::COUNT_FETCH_ALL);
}

} // namespace teleglax

