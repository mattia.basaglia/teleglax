#include "teleglax/objects/input_file.hpp"

#include "teleglax/teleglax.hpp"
#include "teleglax/objects/message.hpp"

namespace teleglax {

void CustomFileGeneration::generate(
    Teleglax* teleglax,
    std::int64_t generation_id,
    const std::string& generation,
    const std::string& path,
    const std::string& output_filename
)
{
    auto generator = map.find(generation);
    if ( generator == map.end() || !generator->second )
    {
        std::string what = "Unknown generator " + generation;
        teleglax->request<td::td_api::finishFileGeneration>(
            generation_id,
            td::td_api::make_object<td::td_api::error>(501, what)
        );
        throw FileGeneratorError(what, generation, path);
    }
    std::ofstream file(output_filename, std::ios::out|std::ios::binary);
    try {
        generator->second(file, path);
    } catch(const std::exception&) {
        std::string what = "Exception during generator";
        teleglax->request<td::td_api::finishFileGeneration>(
            generation_id,
            td::td_api::make_object<td::td_api::error>(500, what)
        );
        throw FileGeneratorError(what, generation, path);
    }
}

} // namespace teleglax
