#ifndef TELEGLAX_GUI_CHAT_MEMBER_DELEGATE_HPP
#define TELEGLAX_GUI_CHAT_MEMBER_DELEGATE_HPP


#include "smegging_delegate.hpp"

class ChatMemberDelegate : public SmeggingDelegate
{
    Q_OBJECT

public:
    ChatMemberDelegate(QWidget *parent = nullptr)
    : SmeggingDelegate(parent)
    {}

    void paint(
        QPainter *painter,
        const QStyleOptionViewItem &option,
        const QModelIndex &index
    ) const override;

    QSize sizeHint(
        const QStyleOptionViewItem &option,
        const QModelIndex &index
    ) const override;

private:
    int collapse_size = 80;
    int min_icon_size = 32;
    int max_icon_size = 48;
};

#endif // TELEGLAX_GUI_CHAT_MEMBER_DELEGATE_HPP

