#ifndef TELEGLAX_GUI_USER_CHAT_MEMBER_MODEL_HPP
#define TELEGLAX_GUI_USER_CHAT_MEMBER_MODEL_HPP


#include <QAbstractListModel>

#include "teleglax/objects/chat_member.hpp"
#include "qt_user.hpp"

class QtChatMember : public RegistrableWrapper<teleglax::ChatMember>
{
public:
    using RegistrableWrapper::RegistrableWrapper;
};

Q_DECLARE_METATYPE(QtChatMember)


class ChatMemberListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    static constexpr auto UserRole = Qt::UserRole;
    static constexpr auto ChatMemberRole = Qt::UserRole + 1;

    int rowCount(const QModelIndex &parent = {}) const override
    {
        std::unique_lock lock{mutex};
        sync_from_future();
        return members.size();
    }

    int columnCount(const QModelIndex &parent = QModelIndex()) const override
    {
        return 1;
    }

    QVariant data(const QModelIndex &index, int role) const override
    {
        teleglax::ChatMember member = get_member(index.row());
        if ( member.is_null() )
            return {};

        switch( role )
        {
            case UserRole:
                return QVariant::fromValue(QtUser(member.user()));
            case ChatMemberRole:
                return QVariant::fromValue(QtChatMember(std::move(member)));
            case Qt::DecorationRole:
                if ( member.user().has_profile_photo() )
                    return QImage(QtUser(member.user()).image_small());
            case Qt::DisplayRole:
            case Qt::ToolTipRole:
            case Qt::StatusTipRole:
            case Qt::AccessibleTextRole:
                return member.user().full_name().c_str();
        }

        return QVariant();
    }

    void set_members(std::vector<teleglax::ChatMember> members)
    {
        beginResetModel();
        std::unique_lock lock{mutex};
        this->members = std::move(members);
        future = {};
        lock.unlock();
        endResetModel();
    }

    void set_members(std::future<std::vector<teleglax::ChatMember>> members)
    {
        beginResetModel();
        std::unique_lock lock{mutex};
        this->members.clear();
        this->future = std::move(members);
        lock.unlock();
        endResetModel();
    }


    void clear()
    {
        beginResetModel();
        std::unique_lock lock{mutex};
        this->members.clear();
        this->future = {};
        lock.unlock();
        endResetModel();
    }
private:
    bool acceptable(int row) const
    {
        std::unique_lock lock{mutex};
        return row >= 0 && row <= int(members.size());
    }

    teleglax::ChatMember get_member(int row) const
    {
        std::unique_lock lock{mutex};
        sync_from_future();
        if ( !acceptable(row) )
            return {nullptr, {}};
        return members[row];
    }

    void sync_from_future() const
    {
        std::unique_lock lock{mutex};
        if ( future.valid() )
        {
            members = future.get();
            future = {};
        }
    }

    mutable std::vector<teleglax::ChatMember> members;
    mutable std::recursive_mutex mutex;
    mutable std::future<std::vector<teleglax::ChatMember>> future;
};


#endif // TELEGLAX_GUI_USER_CHAT_MEMBER_MODEL_HPP
