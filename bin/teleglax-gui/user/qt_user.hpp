#ifndef TELEGLAX_QT_USER_HPP
#define TELEGLAX_QT_USER_HPP

#include <QImage>
#include <QPainter>

#include "teleglax/objects/user.hpp"
#include "registrable_wrapper.hpp"

class QtUser : public RegistrableWrapper<teleglax::User>
{
public:
    using RegistrableWrapper::RegistrableWrapper;

    const QImage& image_small() const
    {
        return get_image(&teleglax::ProfilePhoto::small, image_small_);
    }

    const QImage& image_large() const
    {
        return get_image(&teleglax::ProfilePhoto::large, image_large_);
    }

    /// \todo extract function to share code with qt_chat?
    QRect paint_picture(QPainter& painter, QRect area) const
    {
        auto picsz = std::min(area.width(), area.height());
        if ( picsz == 0 || !has_profile_photo() )
            return QRect{area.topLeft(), area.bottomLeft()};

        if ( picsz > 640 )
            picsz = 640;

        QRect rect = QRect(area.x(), area.y(), picsz, picsz);
        if ( picsz < 160 )
            painter.drawImage(rect, image_small());
        else
            painter.drawImage(rect, image_large());

        return rect;
    }

private:
    const QImage& get_image(teleglax::File (teleglax::ProfilePhoto::*getter)() const, QImage& out) const
    {
        if ( !has_profile_photo() )
        {
            out = {};
        }
        else if ( out.isNull() )
        {
            auto pp = profile_photo();
            teleglax::File file = (pp.*getter)();
            if ( file.is_downloading_completed() )
                out.load(file.path().c_str());
            else if ( file.can_be_downloaded() )
                file.download();
        }
        return out;
    }

    mutable QImage image_small_;
    mutable QImage image_large_;
};

Q_DECLARE_METATYPE(QtUser)


#endif // TELEGLAX_QT_USER_HPP
