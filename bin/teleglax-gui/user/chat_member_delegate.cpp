#include "chat_member_delegate.hpp"

#include <QPainter>

#include "chat_member_list_model.hpp"


void ChatMemberDelegate::paint(
    QPainter *painter,
    const QStyleOptionViewItem &option,
    const QModelIndex &index
) const
{
    if ( index.data(ChatMemberListModel::ChatMemberRole).canConvert<QtChatMember>() )
    {
        QtChatMember chat = qvariant_cast<QtChatMember>(index.data(ChatMemberListModel::ChatMemberRole));
        QtUser user = chat.user();

        painter->save();

        painter->setRenderHint(QPainter::SmoothPixmapTransform);
        painter->setRenderHint(QPainter::Antialiasing);

        if (option.state & QStyle::State_Selected)
        {
            painter->fillRect(option.rect, option.palette.highlight());
            painter->setPen(QPen(option.palette.highlightedText(), 1));
        }
        else
        {
            painter->setPen(QPen(option.palette.text(), 1));
        }

        QRect picrect = user.paint_picture(*painter, option.rect);

        if ( option.rect.width() > collapse_size )
        {
            QRect rect = option.rect;
            rect.setLeft(
                picrect.right() + option.rect.height() / 6
            );

            QFont font = painter->font();
            qreal base_height = painter->fontMetrics().height();
            qreal base_point_size = font.pointSizeF();
            qreal ratio = rect.height() / base_height;

            font.setPointSize(base_point_size * ratio * 0.5);
            painter->setFont(font);
            QFontMetrics metrics = painter->fontMetrics();
            QString name = metrics.elidedText(user.full_name().c_str(), Qt::ElideRight, rect.width());
            painter->drawText(QPoint(0, metrics.ascent()) + rect.topLeft(), name);
        }

        if (option.state & QStyle::State_Selected)
        {
            QPainterPath picselector;
            picselector.setFillRule(Qt::OddEvenFill);
            picselector.addRect(picrect);
            picselector.addEllipse(picrect);
            painter->fillPath(picselector, option.palette.highlight());
        }

        painter->restore();
    }
    else
    {
        QStyledItemDelegate::paint(painter, option, index);
    }
}

QSize ChatMemberDelegate::sizeHint(
    const QStyleOptionViewItem &option,
    const QModelIndex &index
) const
{
    if ( index.data(ChatMemberListModel::ChatMemberRole).canConvert<QtChatMember>() )
    {
        int width = option.rect.width();
        if ( width <= collapse_size )
            return {width, width};
        if ( width < min_icon_size * 4 )
            return {width, min_icon_size};
        if ( width < max_icon_size * 4 )
            return {width, width / 4};
        return {width, max_icon_size};
    }
    else
    {
        return QStyledItemDelegate::sizeHint(option, index);
    }
}
