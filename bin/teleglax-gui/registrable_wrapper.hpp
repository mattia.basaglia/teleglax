#ifndef TELEGLAX_GUI_REGISTRABLE_WRAPPER_HPP
#define TELEGLAX_GUI_REGISTRABLE_WRAPPER_HPP

#include <utility>


template<class Base>
class RegistrableWrapper : public Base
{
public:
    using Base::Base;
    RegistrableWrapper() : Base(nullptr, 0) {}
    RegistrableWrapper(Base oth) : Base(std::move(oth)) {}
    RegistrableWrapper(RegistrableWrapper&& oth) : Base(std::move(oth)) {}
    RegistrableWrapper(const RegistrableWrapper& oth) : Base(oth) {}
    RegistrableWrapper& operator=(RegistrableWrapper&& oth)
    {
        Base::operator=(std::move(oth));
        return *this;
    }
    RegistrableWrapper& operator=(const RegistrableWrapper& oth)
    {
        Base::operator=(oth);
        return *this;
    }
};

#endif // TELEGLAX_GUI_REGISTRABLE_WRAPPER_HPP
