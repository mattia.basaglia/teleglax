#ifndef TELEGLAX_GUI_SMEGGING_DELEGATE_HPP
#define TELEGLAX_GUI_SMEGGING_DELEGATE_HPP

#include <QStyledItemDelegate>

class SmeggingDelegate : public QStyledItemDelegate
{
    Q_OBJECT

public:
    using QStyledItemDelegate::QStyledItemDelegate;

public slots:
    void updatethesmeggingsize(const QModelIndex &index)
    {
        emit sizeHintChanged(index);
    }
};

#endif // TELEGLAX_GUI_SMEGGING_DELEGATE_HPP
