#ifndef TELEGLAXWINDOW_H
#define TELEGLAXWINDOW_H

#include <memory>
#include <QtWidgets/QMainWindow>

class TeleglaxWindowPrivate;

class TeleglaxWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit TeleglaxWindow(QWidget *parent = nullptr, Qt::WindowFlags flags = {});
    ~TeleglaxWindow();

private slots:
    void post_logged_in(bool logged_in);

protected:
    void changeEvent(QEvent *e) override;

private:
    std::unique_ptr<TeleglaxWindowPrivate> d_ptr;
};

#endif // TELEGLAXWINDOW_H
