#include "authdialog.hpp"
#include "ui_authdialog.h"

class AuthDialogPrivate : public Ui_AuthDialog
{
};

AuthDialog::AuthDialog(QWidget *parent, Qt::WindowFlags flags)
    : QDialog(parent, flags),
     d_ptr(std::make_unique<AuthDialogPrivate>())
{
    d_ptr->setupUi(this);
    auto rev = new QRegExpValidator(QRegExp("\\+[0-9 ]{0,15}"), d_ptr->input_phone_number);
    d_ptr->input_phone_number->setValidator(rev);
    connect(d_ptr->button_next, &QPushButton::clicked, this, &AuthDialog::page_confirmed);
}

AuthDialog::~AuthDialog() = default;

void AuthDialog::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch ( e->type() )
    {
        case QEvent::LanguageChange:
            d_ptr->retranslateUi(this);
            break;
        default:
            break;
    }
}

DialogAuth::DialogAuth(QWidget* parent)
    : parent(parent)
{}

DialogAuth::Code DialogAuth::get_code(td::td_api::authorizationStateWaitCode state)
{
    emit auth_request_code(!state.is_registered_);
    show_dialog();
    dialog->d_ptr->container_name->setVisible(!state.is_registered_);
    dialog_loop(dialog->d_ptr->page_auth_code);
    Code code;
    if ( !state.is_registered_ )
    {
        code.first_name = dialog->d_ptr->input_first_name->text().toStdString();
        code.last_name = dialog->d_ptr->input_last_name->text().toStdString();
    }
    code.code = dialog->d_ptr->input_auth_code->text().toStdString();
    return code;
}

std::string DialogAuth::get_password(td::td_api::authorizationStateWaitPassword)
{
    emit auth_password();
    return {};
}

DialogAuth::PhoneNumber DialogAuth::get_phone_number()
{
    emit auth_phone_number();
    show_dialog();
    dialog_loop(dialog->d_ptr->page_phone_number);
    return {dialog->d_ptr->input_phone_number->text().toStdString(), false, false};
}

bool DialogAuth::is_bot()
{
    return false;
}

std::string DialogAuth::get_bot_token()
{
    emit auth_request_bot();
    return {};
}

void DialogAuth::on_auth_complete(bool logged_in)
{
    emit auth_completed(logged_in);
    if ( dialog )
    {
        dialog->close();
        dialog = {};
    }
}

void DialogAuth::show_dialog()
{
    if ( !dialog )
    {
        dialog = std::make_unique<AuthDialog>(parent);
        dialog->setWindowModality(Qt::WindowModal);
        dialog->show();
    }
}

void DialogAuth::dialog_loop(QWidget* page)
{
    dialog->d_ptr->stacked_widget->setCurrentWidget(page);

    QEventLoop loop;
//     dialog->connect(dialog.get(), &AuthDialog::finished, &loop, &QEventLoop::quit);
    dialog->connect(dialog.get(), &AuthDialog::page_confirmed, &loop, &QEventLoop::quit);
    loop.exec();
}

void DialogAuth::on_auth_logging_out()
{
    emit auth_logging_out();
}
