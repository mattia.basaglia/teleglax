#include <QtWidgets/QApplication>
#include <QCommandLineParser>

#include "teleglaxwindow.hpp"
#include "settings.hpp"
#include "teleglax/authorization_source.hpp"
#include "qt_event_trigger.hpp"
#include "chat/qt_chat.hpp"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QCoreApplication::setApplicationName("teleglax");
    QCoreApplication::setApplicationVersion("1.0");

    qRegisterMetaType<teleglax::EventTrigger::ConnectionStatus>();
    qRegisterMetaType<QtChat>();

    settings::load_api();
    QCommandLineParser parser;
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addOption({
        "api-id",
        "API authentication credentials",
        "api-id",
    });
    parser.addOption({
        "api-hash",
        "API authentication credentials",
        "api-hash",
    });
    parser.process(app);
    if ( parser.isSet("api-id") && parser.isSet("api-hash") )
    {
        settings::app_info.api_hash = parser.value("api-hash").toStdString();
        settings::app_info.api_id = parser.value("api-id").toInt();
    }

    TeleglaxWindow window;
    window.show();
    int ret = app.exec();
    settings::save_api();
    return ret;
}
