#ifndef AUTHDIALOG_H
#define AUTHDIALOG_H

#include <QDialog>

#include <memory>

#include "teleglax/authorization_source.hpp"

class AuthDialogPrivate;
class DialogAuth;

class AuthDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AuthDialog(QWidget *parent = nullptr, Qt::WindowFlags flags = {});
    ~AuthDialog();


protected:
    void changeEvent(QEvent *e) override;

signals:
    void page_confirmed();

private:
    std::unique_ptr<AuthDialogPrivate> d_ptr;
    friend DialogAuth;
};


class DialogAuth : public QObject, public teleglax::AuthorizationInput
{
    Q_OBJECT

public:
    explicit DialogAuth(QWidget* parent=nullptr);
    DialogAuth(DialogAuth&& oth)
    : dialog(std::move(oth.dialog)),
      parent(oth.parent)
    {}

    Code get_code(td::td_api::authorizationStateWaitCode state) override;

    std::string get_password(td::td_api::authorizationStateWaitPassword) override;

    PhoneNumber get_phone_number() override;

    bool is_bot() override;

    std::string get_bot_token() override;

    void on_auth_complete(bool logged_in) override;
    void on_auth_logging_out() override;

signals:
    void auth_phone_number();
    void auth_password();
    void auth_request_code(bool new_user);
    void auth_request_bot();
    void auth_completed(bool logged_in);
    void auth_logging_out();


private:
    void dialog_loop(QWidget* page);
    void show_dialog();

    std::unique_ptr<AuthDialog> dialog;
    QWidget* parent;
};
#endif // AUTHDIALOG_H
