#ifndef TELEGLAX_GUI_SETTINGS_HPP
#define TELEGLAX_GUI_SETTINGS_HPP

#include <QSettings>

#include "teleglax/objects/app_info.hpp"

namespace settings {
extern teleglax::AppInfo app_info;

QSettings get_settings();

void load_api();

void save_api();

} // namespace settings
#endif // TELEGLAX_GUI_SETTINGS_HPP
