#include "teleglaxwindow.hpp"
#include "ui_teleglaxwindow.h"

#include <QMessageBox>

#include "teleglax.hpp"
#include "settings.hpp"
#include "authdialog.hpp"
#include "qt_event_trigger.hpp"
#include "chat/chat_delegate.hpp"
#include "chat/chat_list_model.hpp"
#include "user/chat_member_list_model.hpp"
#include "user/chat_member_delegate.hpp"
#include "message/upload_dialog.hpp"

class TeleglaxWindowPrivate : public Ui_TeleglaxWindow
{
public:
    TeleglaxWindowPrivate(TeleglaxWindow* parent)
        : teleglax(settings::app_info, DialogAuth{parent}, std::make_unique<QtEventTrigger>())
    {}

    void send_text()
    {
        send_message(teleglax::msg::Text(msg_text->toTelegramString()));
    }

    void send_message(teleglax::MessageContent msg)
    {
        QVariant data = list_chats->currentIndex().data(ChatListModel::ChatRole);
        if ( data.canConvert<QtChat>() )
        {
            QtChat chat = qvariant_cast<QtChat>(data);
            chat.send_message(std::move(msg));
            msg_text->clear();
        }
    }

    void attach()
    {
        UploadDialog dialog;
        if ( dialog.exec() == QDialog::Accepted )
        {
            send_message(dialog.message_content());
        }
    }

    teleglax::Teleglax teleglax;
    std::thread thread;
    std::atomic<bool> run = true;
};

class SmeggingOvercomplicatedQtViews : public QObject
{
    Q_OBJECT
public:
    using QObject::QObject;

    bool eventFilter(QObject *, QEvent *event) override
    {
        if ( event->type() == QEvent::Resize)
        {
            emit smegging_resize();
        }
        return false;
    }

    static void resize_the_smegger(QListView* view)
    {
        SmeggingDelegate* delegate = static_cast<SmeggingDelegate*>(view->itemDelegate());
        QAbstractItemModel* model = view->model();
        auto soqv = new SmeggingOvercomplicatedQtViews(view);
        view->installEventFilter(soqv);
        connect(soqv, &SmeggingOvercomplicatedQtViews::smegging_resize, view,
            [delegate, model]{
                delegate->updatethesmeggingsize(model->index(0, 0));
        });
    }

signals:
    void smegging_resize();
};
#include "teleglaxwindow.moc"

TeleglaxWindow::TeleglaxWindow(QWidget *parent, Qt::WindowFlags flags)
    : QMainWindow(parent, flags),
      d_ptr(std::make_unique<TeleglaxWindowPrivate>(this))
{
    d_ptr->setupUi(this);

    DialogAuth* auth = static_cast<DialogAuth*>(d_ptr->teleglax.auth_source().source());
    connect(auth, &DialogAuth::auth_completed, this, &TeleglaxWindow::post_logged_in);
    connect(auth, &DialogAuth::auth_request_code, this, [this](bool new_user){
        if ( new_user )
            d_ptr->statusbar->showMessage("Creating user...");
        else
            d_ptr->statusbar->showMessage("Logging in...");
    });
    connect(auth, &DialogAuth::auth_phone_number, this, [this](){
        d_ptr->statusbar->showMessage("Connecting...");
    });
    connect(auth, &DialogAuth::auth_logging_out, this, [this](){
        d_ptr->statusbar->showMessage("Logging out...");
    });
    QtEventTrigger* event_trigger = static_cast<QtEventTrigger*>(d_ptr->teleglax.event_trigger());
    connect(event_trigger,  &QtEventTrigger::connection_status_changed, this,
        [this](teleglax::EventTrigger::ConnectionStatus status){
            switch ( status )
            {
                case teleglax::EventTrigger::ConnectionStatus::WaitingForNetwork:
                    d_ptr->statusbar->showMessage("Waiting for network...");
                    break;
                case teleglax::EventTrigger::ConnectionStatus::ConnectingToProxy:
                    d_ptr->statusbar->showMessage("Connecting to proxy...");
                    break;
                case teleglax::EventTrigger::ConnectionStatus::Connecting:
                    d_ptr->statusbar->showMessage("Connecting...");
                    break;
                case teleglax::EventTrigger::ConnectionStatus::Updating:
                    d_ptr->statusbar->showMessage("Updating...");
                    break;
                case teleglax::EventTrigger::ConnectionStatus::Ready:
                    d_ptr->statusbar->showMessage("Ready.", 5000);
                    break;
            }
    });

    connect(d_ptr->msg_send, &QPushButton::clicked, this, [this]{d_ptr->send_text();});
    connect(d_ptr->msg_attach, &QPushButton::clicked, this, [this]{d_ptr->attach();});

    d_ptr->teleglax.connect();

    d_ptr->thread = std::thread{[this]{
        while ( d_ptr->run )
        {
            try {
                d_ptr->teleglax.poll(1);
            } catch ( const teleglax::UnknownObjectError& e ) {
                /// \todo log errors
            } catch ( const teleglax::ApiError& e ) {
                /// \todo log errors
            } catch ( const std::exception& e ) {
                d_ptr->run = false;
                close();
            }
        }
    }};

    ChatPreviewDelegate* delegate = new ChatPreviewDelegate{};
    d_ptr->list_chats->setItemDelegate(delegate);
    ChatListModel* model = new ChatListModel{&d_ptr->teleglax};
    d_ptr->list_chats->setModel(model);
    connect(event_trigger,  &QtEventTrigger::chat_added, model, &ChatListModel::add_chat);
    connect(event_trigger,  &QtEventTrigger::chat_updated, model, &ChatListModel::update_chat);

    ChatMemberDelegate* member_delegate = new ChatMemberDelegate{};
    d_ptr->list_users->setItemDelegate(member_delegate);
    ChatMemberListModel* member_model = new ChatMemberListModel();
    d_ptr->list_users->setModel(member_model);

    connect(
        d_ptr->list_chats->selectionModel(),
        &QItemSelectionModel::currentChanged,
        this,
        [member_model](const QModelIndex &current, const QModelIndex &){
            QVariant data = current.data(ChatListModel::ChatRole);
            if ( data.canConvert<QtChat>() )
            {
                QtChat chat = qvariant_cast<QtChat>(data);
                member_model->set_members(chat.chat_members());
            }
            else
            {
                member_model->clear();
            }
        }
    );

    // For some reason QListView doesn't update the item size based on the
    // delegate when resized, so we need this weird workaround
    SmeggingOvercomplicatedQtViews::resize_the_smegger(d_ptr->list_chats);
    SmeggingOvercomplicatedQtViews::resize_the_smegger(d_ptr->list_users);
}

TeleglaxWindow::~TeleglaxWindow()
{
    d_ptr->run = false;
    d_ptr->thread.join();
}

void TeleglaxWindow::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch ( e->type() )
    {
        case QEvent::LanguageChange:
            d_ptr->retranslateUi(this);
            break;
        default:
            break;
    }
}

void TeleglaxWindow::post_logged_in(bool logged_in)
{
    if ( !logged_in )
    {
        d_ptr->statusbar->showMessage("Log in failed.");
        QMessageBox::critical(this, "Error", "Log in failed");
        close();
    }
    else
    {
        d_ptr->statusbar->showMessage("Connected.", 5000);
    }
}
