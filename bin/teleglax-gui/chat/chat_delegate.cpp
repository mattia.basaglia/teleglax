#include "chat_delegate.hpp"

#include <QPainter>

#include "qt_chat.hpp"


struct BadgePainter
{
    void push(int value, QColor color)
    {
        if ( value <= 0 )
            return;

        values.push_back({value, color});
    }

    void paint(QPainter* painter, qreal diam, QPoint topright)
    {
        if ( values.empty() )
            return;

        qreal right = topright.x();
        qreal radius = diam / 2;
        std::size_t i = 0;
        auto metrics = painter->fontMetrics();

        for ( auto iter = values.rbegin(); iter != values.rend(); ++iter, ++i )
        {
            QString text = QString::number(iter->first);

            painter->setPen(QPen(Qt::transparent));
            painter->setBrush(iter->second);

            qreal width = std::max(
                painter->boundingRect(QRect(), Qt::TextSingleLine, text).width() + radius,
                diam
            );
            qreal text_width = width;

            if ( i == 0 )
            {
                painter->drawEllipse(
                    right - diam,
                    topright.y(),
                    diam,
                    diam
                );
                right -= radius;
                width -= radius;
            }

            right -= width;

            qreal text_right = right;

            if ( i + 1 == values.size() )
            {
                painter->drawEllipse(
                    right,
                    topright.y(),
                    diam,
                    diam
                );
                right += radius;
                width -= radius;
            }

            painter->drawRect(
                right,
                topright.y(),
                width,
                diam
            );


            painter->setPen(QPen(Qt::white));
            painter->drawText(QRectF(text_right, topright.y(), text_width, diam), Qt::AlignCenter, text);
        }
    }

    std::vector<std::pair<int, QColor>> values;
};


static void paint_unread_badge(const QtChat& chat, QPainter *painter, const QStyleOptionViewItem &option)
{
    int unread = chat.unread_count();
    int unread_mentions = chat.unread_mention_count();

    if ( unread_mentions )
        unread -= unread_mentions;

    if ( !unread && !unread_mentions )
        return;

    qreal unread_diam = option.rect.height() / 3.0;
    painter->setPen(QPen(Qt::transparent));
    QFont font = painter->font();
    font.setPointSizeF(
        font.pointSizeF() * unread_diam / painter->fontMetrics().height()
    );
    font.setBold(true);
    painter->setFont(font);

    BadgePainter bp;
    bp.push(unread, QColor(128, 128, 128));
    bp.push(unread_mentions, QColor(192, 64, 64));
    bp.paint(painter, unread_diam, option.rect.topRight());
}

void ChatPreviewDelegate::paint(
    QPainter *painter,
    const QStyleOptionViewItem &option,
    const QModelIndex &index
) const
{
    if ( index.data(Qt::UserRole).canConvert<QtChat>() )
    {
        QtChat chat = qvariant_cast<QtChat>(index.data(Qt::UserRole));

        painter->save();

        painter->setRenderHint(QPainter::SmoothPixmapTransform);
        painter->setRenderHint(QPainter::Antialiasing);

        if (option.state & QStyle::State_Selected)
        {
            painter->fillRect(option.rect, option.palette.highlight());
            painter->setPen(QPen(option.palette.highlightedText(), 1));
        }
        else
        {
            painter->setPen(QPen(option.palette.text(), 1));
        }

        QRect picrect = chat.paint_picture(*painter, option.rect);

        if ( option.rect.width() > collapse_size )
        {
            QRect rect = option.rect;
            rect.setLeft(
                picrect.right() + option.rect.height() / 6
            );
            chat.paint_preview(*painter, rect);
        }

        if (option.state & QStyle::State_Selected)
        {
            QPainterPath picselector;
            picselector.setFillRule(Qt::OddEvenFill);
            picselector.addRect(picrect);
            picselector.addEllipse(picrect);
            painter->fillPath(picselector, option.palette.highlight());
        }

        paint_unread_badge(chat, painter, option);

        painter->restore();
    }
    else
    {
        QStyledItemDelegate::paint(painter, option, index);
    }
}

QSize ChatPreviewDelegate::sizeHint(
    const QStyleOptionViewItem &option,
    const QModelIndex &index
) const
{
    if ( index.data(Qt::UserRole).canConvert<QtChat>() )
    {
        int width = option.rect.width();
        if ( width <= collapse_size )
            return {width, width};
        if ( width < min_icon_size * 4 )
            return {width, min_icon_size};
        if ( width < max_icon_size * 4 )
            return {width, width / 4};
        return {width, max_icon_size};
    }
    else
    {
        return QStyledItemDelegate::sizeHint(option, index);
    }
}
