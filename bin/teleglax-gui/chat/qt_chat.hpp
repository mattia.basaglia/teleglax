#ifndef TELEGLAX_GUI_CHAT_QCHAT_HPP
#define TELEGLAX_GUI_CHAT_QCHAT_HPP

#include <QObject>
#include <QImage>

#include "teleglax/objects/chat.hpp"
#include "registrable_wrapper.hpp"

class QtChat : public RegistrableWrapper<teleglax::Chat>
{
public:
    using RegistrableWrapper::RegistrableWrapper;

    const QImage& image_small() const
    {
        return get_image(&Chat::chat_image_small, image_small_);
    }

    const QImage& image_large() const
    {
        return get_image(&Chat::chat_image_large, image_large_);
    }

    void paint_preview(QPainter& painter, QRect area) const;
    QRect paint_picture(QPainter& painter, QRect area) const;


private:
    const QImage& get_image(teleglax::File (teleglax::Chat::*getter)() const, QImage& out) const
    {
        if ( !has_chat_image() )
        {
            out = {};
        }
        else if ( out.isNull() )
        {
            teleglax::File file = (this->*getter)();
            if ( file.is_downloading_completed() )
                out.load(file.path().c_str());
            else if ( file.can_be_downloaded() )
                file.download();
        }
        return out;
    }

    mutable QImage image_small_;
    mutable QImage image_large_;
};

Q_DECLARE_METATYPE(QtChat)

#endif // TELEGLAX_GUI_CHAT_QCHAT_HPP
