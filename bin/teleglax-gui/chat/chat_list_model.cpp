#include "chat_list_model.hpp"

void ChatListModel::add_chat(const QtChat& chat)
{
    update_chat(chat);
}

void ChatListModel::update_chat(const QtChat& chat)
{
    std::unique_lock lock{mutex};
    auto it = chats.begin();
    auto sz = chats.size();
    auto old_offset = sz;
    auto new_offset = sz;
    std::int64_t order;
    try {
        order = chat.order();
    } catch (...) {
        return;
    }
    while ( it != chats.end() && (new_offset == sz || old_offset == sz) )
    {
        if ( it->id() == chat.id() )
            old_offset = it - chats.begin();
        if ( it->order() <= order && new_offset == sz )
            new_offset = it - chats.begin();
        ++it;
    }

    // not found
    if ( old_offset == sz )
    {
        beginInsertRows(QModelIndex{}, new_offset, new_offset);
        chats.insert(chats.begin() + new_offset, chat);
        endInsertRows();
    }
    // updated, same order
    else if ( old_offset == new_offset )
    {
        chats[old_offset] = chat;
        emit dataChanged(index(old_offset, 0), index(old_offset, 0));
    }
    // found, move to last
    else if ( new_offset == sz )
    {
        beginMoveRows({}, old_offset, old_offset, {}, new_offset - 1);
        chats.erase(chats.begin() + old_offset);
        chats.push_back(chat);
        endMoveRows();
    }
    // found, moved
    else
    {
        beginResetModel();
        // for some reason using
        // beginMoveRows({}, old_offset, old_offset, {}, new_offset);
        // causes a segmentation fault (on endMoveRows())
        if ( old_offset < new_offset )
        {
            std::move(chats.begin() + old_offset + 1, chats.begin() + new_offset + 1, chats.begin() + old_offset);
            chats[new_offset] = chat;
        }
        else
        {
            std::move_backward(chats.begin() + new_offset, chats.begin() + old_offset, chats.begin() + old_offset + 1);
        }
        chats[new_offset] = chat;
        endResetModel();
    }
}

QVariant ChatListModel::data(const QModelIndex &index, int role) const
{
    QtChat chat = get_chat(index.row());
    switch( role )
    {
        case ChatRole:
            return QVariant::fromValue(std::move(chat));
        case IdRole:
            return static_cast<long long>(chat.id());
        case Qt::DecorationRole:
            if ( chat.has_chat_image() )
                return QImage(chat.image_small());
        case Qt::DisplayRole:
        case Qt::ToolTipRole:
        case Qt::StatusTipRole:
        case Qt::AccessibleTextRole:
            return chat.title().c_str();
    }

    return QVariant();
}
