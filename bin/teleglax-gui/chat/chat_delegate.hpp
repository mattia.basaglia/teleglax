#ifndef TELEGLAX_GUI_CHAT_DELEGATE
#define TELEGLAX_GUI_CHAT_DELEGATE

#include "smegging_delegate.hpp"


class ChatPreviewDelegate : public SmeggingDelegate
{
    Q_OBJECT

public:
    ChatPreviewDelegate(QWidget *parent = nullptr)
    : SmeggingDelegate(parent)
    {}

    void paint(
        QPainter *painter,
        const QStyleOptionViewItem &option,
        const QModelIndex &index
    ) const override;

    QSize sizeHint(
        const QStyleOptionViewItem &option,
        const QModelIndex &index
    ) const override;

private:
    int collapse_size = 80;
    int min_icon_size = 32;
    int max_icon_size = 48;
};

#endif // TELEGLAX_GUI_CHAT_DELEGATE
