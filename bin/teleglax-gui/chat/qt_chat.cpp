#include "qt_chat.hpp"

#include <QPainter>
#include <QStyle>

#include "teleglax/objects/message.hpp"


static QString message_type_text(teleglax::Message::ContentType type)
{
    switch ( type )
    {
        default:
        case teleglax::Message::ContentType::Text: return "";
        case teleglax::Message::ContentType::Animation: return "Animation";
        case teleglax::Message::ContentType::Audio: return "Audio";
        case teleglax::Message::ContentType::Document: return "Document";
        case teleglax::Message::ContentType::Photo: return "Photo";
        case teleglax::Message::ContentType::ExpiredPhoto: return "Expired Photo";
        case teleglax::Message::ContentType::Sticker: return "Sticker";
        case teleglax::Message::ContentType::Video: return "Video";
        case teleglax::Message::ContentType::ExpiredVideo: return "Expired Video";
        case teleglax::Message::ContentType::VideoNote: return "Video Note";
        case teleglax::Message::ContentType::VoiceNote: return "Voice Note";
        case teleglax::Message::ContentType::Location: return "Location";
        case teleglax::Message::ContentType::Venue: return "Venue";
        case teleglax::Message::ContentType::Contact: return "Contact";
        case teleglax::Message::ContentType::Game: return "Game";
        case teleglax::Message::ContentType::Invoice: return "Invoice";
        case teleglax::Message::ContentType::Call: return "Call";
        case teleglax::Message::ContentType::BasicGroupChatCreate: return "Created Group";
        case teleglax::Message::ContentType::SupergroupChatCreate: return "Created supergroup";
        case teleglax::Message::ContentType::ChatChangeTitle: return "Changed Title";
        case teleglax::Message::ContentType::ChatChangePhoto: return "Changed Chat Photo";
        case teleglax::Message::ContentType::ChatDeletePhoto: return "Deleted Chat Photo";
        case teleglax::Message::ContentType::ChatAddMembers: return "Added Members";
        case teleglax::Message::ContentType::ChatJoinByLink: return "Joined";
        case teleglax::Message::ContentType::ChatDeleteMember: return "Deleted Member";
        case teleglax::Message::ContentType::ChatUpgradeTo: return "Upgraded Chat";
        case teleglax::Message::ContentType::ChatUpgradeFrom: return "Upgraded Chat";
        case teleglax::Message::ContentType::PinMessage: return "Pinned";
        case teleglax::Message::ContentType::ScreenshotTaken: return "Screenshot";
        case teleglax::Message::ContentType::ChatSetTtl: return "Set Ttl";
        case teleglax::Message::ContentType::CustomServiceAction: return "Service Action";
        case teleglax::Message::ContentType::GameScore: return "Game Score";
        case teleglax::Message::ContentType::PaymentSuccessful: return "Payment";
        case teleglax::Message::ContentType::PaymentSuccessfulBot: return "Payment";
        case teleglax::Message::ContentType::ContactRegistered: return "Registered Contact";
        case teleglax::Message::ContentType::WebsiteConnected: return "Connected Website";
        case teleglax::Message::ContentType::Unsupported: return "Unsupported Message";
    }
}

static bool draw_text(QString text, int& text_x, int text_y, int right, int height, QPainter& painter)
{
    if ( text_x >= right )
        return false;
    QFontMetrics metrics = painter.fontMetrics();
    text = metrics.elidedText(text, Qt::ElideRight, right - text_x);
    painter.drawText(QPoint(text_x, height - metrics.descent() + text_y), text);
    text_x += metrics.width(text);
    return true;
}

static void paint_message(const teleglax::Message& message, QRect area, QPainter& painter)
{
    if ( message.is_null() )
        return;

    QString prefix = message.sender().full_name().c_str();

    QFont font = painter.font();
    font.setWeight(QFont::Bold);
    painter.setFont(font);
    int text_x = area.x();
    int prefix_right = area.x() + area.width() / 2;

    if ( draw_text(prefix, text_x, area.y(), prefix_right, area.height(), painter) )
    {
        font.setWeight(QFont::Normal);
        painter.setFont(font);
        draw_text(": ", text_x, area.y(), area.width(), area.height(), painter);
    }

    prefix = message_type_text(message.content_type());
    if ( !prefix.isEmpty() )
    {
        prefix += ' ';
        font.setWeight(QFont::Bold);
        painter.setFont(font);
        draw_text(prefix, text_x, area.y(), area.right(), area.height(), painter);
    }

    font.setWeight(QFont::Normal);
    painter.setFont(font);
    draw_text(message.content_text().c_str(), text_x, area.y(), area.right(), area.height(), painter);
}

QRect QtChat::paint_picture(QPainter& painter, QRect area) const
{
    auto picsz = std::min(area.width(), area.height());
    if ( picsz == 0 || !has_chat_image() )
        return QRect{area.topLeft(), area.bottomLeft()};

    if ( picsz > 640 )
        picsz = 640;

    QRect rect = QRect(area.x(), area.y(), picsz, picsz);
    if ( picsz < 160 )
        painter.drawImage(rect, image_small());
    else
        painter.drawImage(rect, image_large());

    return rect;
}

void QtChat::paint_preview(QPainter& painter, QRect area) const
{
    QFont font = painter.font();
    qreal base_height = painter.fontMetrics().height();
    qreal base_point_size = font.pointSizeF();
    qreal ratio = area.height() / base_height;

    font.setPointSize(base_point_size * ratio * 0.5);
    font.setWeight(QFont::Bold);
    painter.setFont(font);
    QFontMetrics metrics = painter.fontMetrics();
    QString title = metrics.elidedText(this->title().c_str(), Qt::ElideRight, area.width());
    painter.drawText(QPoint(0, metrics.ascent()) + area.topLeft(), title);

    font.setPointSize(base_point_size * ratio * 0.4);
    painter.setFont(font);
    paint_message(last_message(), area, painter);
}
