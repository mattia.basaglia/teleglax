#ifndef TELEGLAX_GUI_CHAT_LIST_MODEL_HPP
#define TELEGLAX_GUI_CHAT_LIST_MODEL_HPP

#include <mutex>

#include <QAbstractListModel>

#include "qt_chat.hpp"


class ChatListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    static constexpr auto ChatRole = Qt::UserRole;
    static constexpr auto IdRole = Qt::UserRole + 1;

    explicit ChatListModel(teleglax::Teleglax* source)
    : source(source) {}

    int rowCount(const QModelIndex &parent = {}) const override
    {
        maybe_refresh_vector();
        return chats.size();
    }

    int columnCount(const QModelIndex &parent = QModelIndex()) const override
    {
        return 1;
    }

    QVariant data(const QModelIndex &index, int role) const override;

public slots:
    void add_chat(const QtChat& chat);

    void update_chat(const QtChat& chat);

private:
    bool acceptable(int row) const
    {
        std::unique_lock lock{mutex};
        return row >= 0 && row <= int(chats.size());
    }

    QtChat get_chat(int row) const
    {
        std::unique_lock lock{mutex};
        maybe_refresh_vector();

        if ( !acceptable(row) )
            return {source, 0};

        return chats[row];
    }

    void refresh_vector() const
    {
        std::unique_lock lock{mutex};
        chats.clear();
        chats.reserve(source->chats().size());
        for ( auto chat : source->get_chats_range() )
            chats.push_back(chat);
    }

    void maybe_refresh_vector() const
    {
        std::unique_lock lock{mutex};
        if ( chats.empty() )
            refresh_vector();
    }

    teleglax::Teleglax* source;
    mutable std::vector<QtChat> chats;
    mutable std::recursive_mutex mutex;
};

#endif // TELEGLAX_GUI_CHAT_LIST_MODEL_HPP
