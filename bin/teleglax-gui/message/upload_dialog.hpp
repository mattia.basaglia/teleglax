#ifndef TELEGLAX_GUI_MESSAGE_UPLOAD_DIALOG_HPP
#define TELEGLAX_GUI_MESSAGE_UPLOAD_DIALOG_HPP

#include <memory>
#include <QDialog>
#include "teleglax/objects/message_content.hpp"


Q_DECLARE_METATYPE(teleglax::msg::Type)

class UploadDialogPrivate;


class UploadDialog : public QDialog
{
    Q_OBJECT

public:
    explicit UploadDialog(QWidget *parent = nullptr, Qt::WindowFlags flags = {});
    ~UploadDialog();

    teleglax::MessageContent message_content() const;

public slots:
    void set_preview_path(QString path);

protected:
    void changeEvent(QEvent *e) override;

private:
    std::unique_ptr<UploadDialogPrivate> d_ptr;
};

#endif // TELEGLAX_GUI_MESSAGE_UPLOAD_DIALOG_HPP
