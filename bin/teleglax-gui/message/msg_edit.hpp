#ifndef TELEGLAX_GUI_MSG_EDIT_HPP
#define TELEGLAX_GUI_MSG_EDIT_HPP

#include <QTextEdit>
#include "teleglax/objects/message_content.hpp"

class MsgEdit: public QTextEdit
{
public:
    explicit MsgEdit(QWidget *parent = nullptr)
        : MsgEdit({}, parent)
    {}

    explicit MsgEdit(const QString &text, QWidget *parent = nullptr);

    QSize sizeHint() const override;
    QSize minimumSizeHint() const override;

    teleglax::FormattedText toTelegramString() const;

    void contextMenuEvent(QContextMenuEvent *event) override;

protected:
    void resizeEvent(QResizeEvent *event) override
    {
        updateGeometry();
        QTextEdit::resizeEvent(event);
    }
};

#endif // TELEGLAX_GUI_MSG_EDIT_HPP
