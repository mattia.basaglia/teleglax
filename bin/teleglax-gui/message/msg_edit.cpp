#include "msg_edit.hpp"

#include <QContextMenuEvent>
#include <QMenu>

MsgEdit::MsgEdit(const QString &text, QWidget *parent)
    : QTextEdit(text, parent)
{
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    connect(this, &MsgEdit::textChanged, this, [this]{updateGeometry();});
}

QSize MsgEdit::sizeHint() const
{
    return {
        QTextEdit::sizeHint().width(),
        std::max(
            document()->size().toSize().height(),
            fontMetrics().lineSpacing()
        )
    };
}

QSize MsgEdit::minimumSizeHint() const
{
    return {
        QTextEdit::minimumSizeHint().width(),
        sizeHint().height()
    };
}

template<class Func>
QAction* add_action(QMenu *menu, QString icon, QString label, QString key_sequence, bool enabled, Func&& func)
{
    QAction* action = menu->addAction(QIcon::fromTheme(icon), label, std::forward<Func>(func), QKeySequence(key_sequence));
    action->setEnabled(enabled);
    return action;
}

void MsgEdit::contextMenuEvent(QContextMenuEvent *event)
{
    QMenu *menu = createStandardContextMenu(event->globalPos());
    if (!menu)
        return;

    if ( textInteractionFlags() & (Qt::TextEditable | Qt::TextSelectableByKeyboard | Qt::TextSelectableByMouse) )
    {

        menu->addSeparator();
        bool enable_actions = textCursor().hasSelection();
        add_action(menu, "format-text-bold",    tr("&Bold"),            "Ctrl+B",       enable_actions, [this]{
            QTextCharFormat format;
            format.setFontWeight(QFont::Bold);
            textCursor().mergeCharFormat(format);
        });
        add_action(menu, "format-text-italic",  tr("&Italic"),          "Ctrl+I",       enable_actions, [this]{
            QTextCharFormat format;
            format.setFontItalic(true);
            textCursor().mergeCharFormat(format);
        });
        add_action(menu, "text-x-csrc"       ,  tr("Mono&space"),       "Ctrl+Shift+M", enable_actions, [this]{
            QTextCharFormat format;
            format.setFontFamily("monospace");
            textCursor().mergeCharFormat(format);
        });
        add_action(menu, "edit-clear",          tr("&Clear Formatting"),"Ctrl+Shift+N", enable_actions, [this]{
            textCursor().setCharFormat(QTextCharFormat());
        });
        menu->addSeparator();
        add_action(menu, "insert-link",         tr("Insert &Link"),     "Ctrl+K",       true, []{});
        add_action(menu, "user-identity",       tr("&Mention User"),    "Ctrl+M",       true, []{});
    }

    menu->setAttribute(Qt::WA_DeleteOnClose);
    menu->popup(event->globalPos());
}

teleglax::FormattedText MsgEdit::toTelegramString() const
{
    /// \todo Formatting
    return toPlainText().toStdString();
}
