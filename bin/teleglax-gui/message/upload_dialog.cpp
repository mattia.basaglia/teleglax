#include "upload_dialog.hpp"
#include "ui_upload_dialog.h"

#include <QMimeType>
#include <QMimeDatabase>
#include <QListView>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QMediaPlaylist>
#include <QMediaPlayer>
#include <QFileDialog>


using teleglax::msg::Type;


class UploadDialogPrivate : public Ui_UploadDialog
{
public:
    enum MediaTypeTrait
    {
        Audio       = 0x0001,
        Video       = 0x0002,
        Image       = 0x0004,
        Other       = 0x0010,
        TypeMask    = 0x00ff,

        Caption     = 0x0100
    };

    QUrl uri;
    QMimeType mime_type;
    QListView * combo_view = nullptr;
    QNetworkAccessManager network;
    MediaTypeTrait main_trait = Other;
    QMediaPlayer player;
    QMediaPlaylist playlist{&player};

    void downloaded(QNetworkReply *reply)
    {
        if (reply->error() != QNetworkReply::NoError)
        {
            qDebug() << "Error in" << reply->url() << ":" << reply->errorString();
            return;
        }

        QByteArray data = reply->readAll();

        if ( main_trait & Image )
        {
            QPixmap pixmap;
            pixmap.loadFromData(data);
            set_preview_image(pixmap);
        }
    }

    void set_preview_image(const QPixmap& pix)
    {
        preview_image->setPixmap(pix);
    }

    void combo_activated(int index)
    {
        int trait = media_type_trait(combo_type->itemData(index).value<Type>());
        caption_edit->setVisible(trait & Caption);
    }

    int media_type_trait(Type type)
    {
        switch ( type )
        {
            case Type::Document:    return Other|Caption;
            case Type::Photo:       return Image|Caption;
            case Type::Sticker:     return Image;
            case Type::Video:       return Video|Caption;
            case Type::VideoNote:   return Video;
            case Type::Animation:   return Video|Caption;
            case Type::Audio:       return Audio|Caption;
            case Type::VoiceNote:   return Audio|Caption;
            default:                return Other|Caption;
        }
    }

    QList<Type> allowed_types(const QMimeType& mime)
    {
        if ( mime.inherits("image/gif") )
            return { Type::Animation, Type::Document };
        if ( mime.name().startsWith("image/") )
            return { Type::Photo, Type::Document, Type::Sticker };
        if ( mime.name().startsWith("video/") )
            return { Type::Video, Type::Document, Type::VideoNote, Type::Animation };
        if ( mime.name().startsWith("audio/") )
            return { Type::Audio, Type::Document, Type::VoiceNote };
        return { Type::Document };
    }

    void set_uri(const QString& path)
    {
        uri =  QUrl(path);
        if ( uri.scheme().isEmpty() )
            uri.setScheme("file");
    }
};



UploadDialog::UploadDialog(QWidget *parent, Qt::WindowFlags flags)
    : QDialog(parent, flags),
      d_ptr(std::make_unique<UploadDialogPrivate>())
{
    d_ptr->setupUi(this);
    d_ptr->preview_image->setVisible(false);
    d_ptr->preview_video->setVisible(false);
    d_ptr->caption_edit->setVisible(false);
    d_ptr->player.setVideoOutput(d_ptr->preview_video);


    d_ptr->combo_view = new QListView(d_ptr->combo_type);
    d_ptr->combo_type->setView(d_ptr->combo_view);

    d_ptr->combo_type->setVisible(false);
    d_ptr->combo_type->addItem(QIcon::fromTheme("text-plain"), "Document", (long long) Type::Document);

    d_ptr->combo_type->addItem(QIcon::fromTheme("image-x-generic"), "Photo (compressed)", (long long) Type::Photo);
    d_ptr->combo_type->addItem(QIcon::fromTheme("face-smile"), "Sticker", (long long) Type::Sticker);

    d_ptr->combo_type->addItem(QIcon::fromTheme("video-x-generic"), "Video", (long long) Type::Video);
    d_ptr->combo_type->addItem(QIcon::fromTheme("camera-web"), "Video Note", (long long) Type::VideoNote);
    d_ptr->combo_type->addItem(QIcon::fromTheme("camera-web"), "Animation (Gif)", (long long) Type::Animation);

    d_ptr->combo_type->addItem(QIcon::fromTheme("audio-x-generic"), "Audio", (long long) Type::Audio);
    d_ptr->combo_type->addItem(QIcon::fromTheme("audio-input-microphone"), "Voice Note", (long long) Type::VoiceNote);

    connect(d_ptr->file_ok, &QPushButton::clicked, this, [this]{
        set_preview_path(d_ptr->file_edit->text());
    });
    connect(d_ptr->file_browse, &QPushButton::clicked, this, [this]{
        d_ptr->file_edit->setText(
            QFileDialog::getOpenFileName(this, "Open file", {},
                "All Files (*);; "
                "Images (*.png *.jpg *.jpeg *.bmp *.webp);;"
                "Videos (*.mp4 *.gif *.ogv *.webm);;"
                "Audio (*.mp3 *.ogg *.ogg);;"
            )
        );
        set_preview_path(d_ptr->file_edit->text());
    });
}

UploadDialog::~UploadDialog() = default;

void UploadDialog::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch ( e->type() )
    {
        case QEvent::LanguageChange:
            d_ptr->retranslateUi(this);
            break;
        default:
            break;
    }
}

void UploadDialog::set_preview_path(QString path)
{
    d_ptr->set_uri(path);
    QMimeDatabase db;

    d_ptr->mime_type = db.mimeTypeForUrl(d_ptr->uri);

    QList<Type> types = d_ptr->allowed_types(d_ptr->mime_type);
    int default_type = 0;
    for ( int i = 0; i < d_ptr->combo_type->count(); i++ )
    {
        Type type = d_ptr->combo_type->itemData(i).value<Type>();
        if ( type == types[0] )
            default_type = i;
        bool present = types.contains(type);
        d_ptr->combo_view->setRowHidden(i, !present);
    }

    d_ptr->preview_image->setVisible(false);
    d_ptr->preview_video->setVisible(false);
    d_ptr->caption_edit->setVisible(false);

    for ( Type type : types )
    {
        if ( d_ptr->media_type_trait(type) & UploadDialogPrivate::Image )
        {
            d_ptr->preview_image->setVisible(true);
            if ( d_ptr->uri.isLocalFile() )
                d_ptr->set_preview_image(QPixmap(d_ptr->uri.path()));
            else
                d_ptr->network.get(QNetworkRequest(d_ptr->uri));
            break;
        }

        if ( d_ptr->media_type_trait(type) & UploadDialogPrivate::Video )
        {
            d_ptr->preview_video->setVisible(true);
            d_ptr->playlist.clear();
            d_ptr->playlist.addMedia(d_ptr->uri);
            d_ptr->playlist.setCurrentIndex(0);
            break;
        }
    }
    d_ptr->combo_type->setCurrentIndex(default_type);
    d_ptr->combo_type->setVisible(true);
}

teleglax::MessageContent UploadDialog::message_content() const
{
    teleglax::InputFile infile =
        d_ptr->uri.isLocalFile() ?
        teleglax::InputFile{teleglax::InputFile::Type::Path, d_ptr->uri.path().toStdString()} :
        teleglax::InputFile{teleglax::InputFile::Type::Url, d_ptr->uri.toString().toStdString()}
    ;
    teleglax::FormattedText caption = d_ptr->caption_edit->toTelegramString();

    switch ( d_ptr->combo_type->currentData().value<Type>() )
    {
        default:
        case Type::Document:
            return teleglax::msg::Document(infile, caption);
        case Type::Photo:
            return teleglax::msg::Photo(infile, caption);
        case Type::Sticker:
            return teleglax::msg::Sticker(infile, caption);
        case Type::Video:
            return teleglax::msg::Video(infile, caption);
        case Type::VideoNote:
            return teleglax::msg::VideoNote(infile, caption);
        case Type::Animation:
            return teleglax::msg::Animation(infile, caption);
        case Type::Audio:
            return teleglax::msg::Audio(infile, caption);
        case Type::VoiceNote:
            return teleglax::msg::VoiceNote(infile, caption);
    }
}
