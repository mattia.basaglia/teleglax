#include "settings.hpp"
#include "td/telegram/Log.h"

#include <QStandardPaths>
#include <QtWidgets/QApplication>

namespace settings {

teleglax::fs::path base_path()
{
    return QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation).toStdString();
}

QSettings get_settings()
{
    return QSettings((base_path() / "gui" / "settings.ini").c_str(), QSettings::IniFormat);
}

teleglax::AppInfo app_info {};


void load_api()
{
    QSettings settings = get_settings();

    settings.beginGroup("api");

    if ( settings.contains("hash") )
        app_info.api_hash = settings.value("hash").toString().toStdString();

    if ( settings.contains("id") )
        app_info.api_id = settings.value("id").toInt();

    app_info.application_version = QCoreApplication::applicationVersion().toStdString();

    app_info.use_test_environment = settings.value("use_test_environment", false).toBool();
    app_info.use_secret_chats = settings.value("use_secret_chats", true).toBool();

    app_info.database_directory = settings.value(
        "database_directory",
        (base_path() / "db" / "teleglax-gui").c_str()
    ).toString().toStdString();
    app_info.files_directory = settings.value(
        "files_directory",
        (base_path() / "db" / "teleglax-gui").c_str()
    ).toString().toStdString();
    app_info.use_file_database = true;
    app_info.use_chat_info_database = true;
    app_info.use_message_database = true;
    app_info.enable_storage_optimizer = true;

    auto langs = QLocale::system().uiLanguages();
    if ( langs.empty() )
        app_info.system_language_code = langs[0].toStdString();
    app_info.device_model = QSysInfo::productType().toStdString();
    app_info.system_version = QSysInfo::productVersion().toStdString();

    td::Log::set_verbosity_level(settings.value("verbosity", 1).toInt());
}

void save_api()
{
    QSettings settings = get_settings();
    settings.beginGroup("api");
    settings.setValue("hash", app_info.api_hash.c_str());
    settings.setValue("id", app_info.api_id);

    settings.setValue("use_test_environment", app_info.use_test_environment);
    settings.setValue("use_secret_chats", app_info.use_secret_chats);
    settings.setValue("database_directory", app_info.database_directory.c_str());
    settings.setValue("files_directory", app_info.files_directory.c_str());
    //settings.setValue("verbosity", td::Log::get_verbosity_level());
}

} // namespace settings
