#ifndef TELEGLAX_GUI_QEVENT_TRIGGER_HPP
#define TELEGLAX_GUI_QEVENT_TRIGGER_HPP


#include <QObject>

#include "teleglax/event_trigger.hpp"
#include "teleglax/objects/message.hpp"

#include "chat/qt_chat.hpp"

Q_DECLARE_METATYPE(teleglax::EventTrigger::ConnectionStatus)

class QtEventTrigger : public QObject, public teleglax::EventTrigger
{
    Q_OBJECT

public:
    void on_connection_status(teleglax::EventTrigger::ConnectionStatus status) override
    {
        emit connection_status_changed(status);
    }

    void on_new_chat(const teleglax::Chat& chat) override
    {
        emit chat_added(chat);
    }

    void on_chat_changed(const teleglax::Chat& chat) override
    {
        emit chat_updated(chat);
    }

signals:
    void connection_status_changed(teleglax::EventTrigger::ConnectionStatus status);
    void chat_added(const QtChat& chat);
    void chat_updated(const QtChat& chat);
};

#endif // TELEGLAX_GUI_QEVENT_TRIGGER_HPP
