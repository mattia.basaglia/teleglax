#include <atomic>
#include <thread>
#include <iostream>
#include <regex>
#include <functional>
#include <queue>

#include <td/telegram/Log.h>

#include "teleglax.hpp"

class StdinAuth : public teleglax::AuthorizationInput
{
public:
    Code get_code(td::td_api::authorizationStateWaitCode state) override
    {
        Code code;
        if ( !state.is_registered_ )
        {
            code.first_name = get_string("Enter your first name: ");
            code.last_name = get_string("Enter your last name: ");
        }
        code.code = get_string("Enter authentication code: ");
        return code;
    }

    std::string get_password(td::td_api::authorizationStateWaitPassword) override
    {
        return get_string("Enter authentication password: ");
    }

    PhoneNumber get_phone_number() override
    {
        std::string phone_number = get_string("Enter phone number: ");
        return {phone_number, false, false};
    }

    bool is_bot() override
    {
        return get_string("User type [bot|user] (default user): ") == "bot";
    }

    std::string get_bot_token() override
    {
        return get_string("Enter authentication token: ");
    }

    void on_auth_complete(bool logged_in) override
    {
        std::cout << "Logged " << ( logged_in ? "in" : "out" ) << '\n';
    }

private:
    std::string get_string(const std::string& prompt)
    {
        std::cout << prompt;
        std::cout.flush();
        std::string result;
        std::getline(std::cin, result);
        return result;
    }
};

class BaseCommand
{
public:
    BaseCommand(std::regex pattern)
        : pattern(std::move(pattern))
    {}

    bool match(const std::string& line, std::smatch& match) const
    {
        return std::regex_match(line, match, pattern);
    }

    virtual void exec(const std::smatch& args) = 0;

private:
    std::regex pattern;
};

class LambdaCommand : public BaseCommand
{
public:
    LambdaCommand(std::regex pattern, std::function<void(const std::smatch& args)> func)
        : BaseCommand(std::move(pattern)),
          func(std::move(func))
    {}

    void exec(const std::smatch& args) override
    {
        func(args);
    }

private:
    std::function<void(const std::smatch& args)> func;

};

class CommandRunner
{
public:
    template<class Callable>
    void add_command(std::string pattern, Callable&& callable)
    {
        commands.emplace_back(std::make_unique<LambdaCommand>(
            std::regex{std::move(pattern), std::regex::ECMAScript},
            std::forward<Callable>(callable)
        ));
    }

    template<class Command, class... Args>
    void add_command(Args&&... args)
    {
        commands.emplace_back(std::make_unique<Command>(
            std::forward<Args>(args)...
        ));
    }

    void run(const std::string& cmd) const
    {
        std::smatch match;
        for( const auto& p : commands )
            if ( p->match(cmd, match) )
            {
                p->exec(match);
                break;
            }
    }

private:
    std::vector<std::unique_ptr<BaseCommand>> commands;
};

class Shell
{
private:
    class ListCommand : public BaseCommand
    {
    public:
        ListCommand(teleglax::Teleglax* tg)
        : BaseCommand(std::regex("(?:/list|/l|/ls)(?:\\s+(\\+))?(?:\\s+([0-9]+))?")),
          last(tg, 0) {}

        void exec(const std::smatch& match) override
        {
            bool next = match.length(1);
            int nchats = match.length(2) ? std::stoi(match[2]) : 10;
            if ( !next )
                last = teleglax::Chat(last.client(), 0);
            auto chats = last.client().get_chats(nchats, last).get();
            for ( const auto& chat : chats )
                std::cout << " * " << chat.id() << " " << chat.title() << '\n';
            if ( !chats.empty() )
                last = chats.back();
        }

    private:
        teleglax::Chat last;
    };

    class EventTrigger : public teleglax::EventTrigger
    {
    public:
        EventTrigger(Shell* shell) : shell(shell) {}

        void on_new_message(const teleglax::Message& msg, bool) override
        {
            if ( shell->current_chat == msg.chat() )
                shell->print_message(msg);
        }

        Shell* shell;
    };

public:
    Shell(teleglax::AppInfo app_info)
        : teleglax(app_info, StdinAuth(), std::make_unique<EventTrigger>(this))
    {
//         teleglax.register_file_generation("#url#",
//         [](std::ostream& out, const std::string& path)
//         {
//             // todo
//         });

        commands.add_command("/quit|/q",
            [this](const std::smatch&){
                run = false;
        });
        commands.add_command("(?:/chat|/c) ([-]?[0-9]+)",
            [this](const std::smatch& match){
                current_chat = teleglax::Chat(teleglax, std::stoll(match[1]));
                if ( current_chat )
                {
                    std::cout << "Chatting on " << current_chat.title() << '\n';
                    teleglax::Message pinned = current_chat.pinned_message().get();
                    if ( pinned )
                    {
                        std::cout << "   Pinned message: ";
                        print_message(pinned);
                    }
                    for ( const auto& msg : current_chat.get_history(10).get() )
                        print_message(msg);
                }
        });
        commands.add_command<ListCommand>(&teleglax);
        commands.add_command("([^/].*)",
            [this](const std::smatch& match){
                if ( current_chat )
                {
                    current_chat.send_message(teleglax::msg::Text(match[1]));
                }
        });
        commands.add_command("/options|/o",
            [this](const std::smatch&){
                for ( const auto& opt : teleglax.options() )
                    std::cout << opt.first << ": " << opt.second << '\n';
        });
        commands.add_command("/logout",
            [this](const std::smatch&){
                teleglax.logout();
        });
        commands.add_command("(?:/search|/s)\\s+(.*)",
            [this](const std::smatch& match){
                if ( current_chat )
                {
                    for ( const auto& msg : current_chat.search(match[1], 10).get() )
                        print_message(msg);
                }
        });
        commands.add_command("(/sticker|/photo|/upload)\\s+(([a-z]+://)?(.+))",
            [this](const std::smatch& match){
                if ( current_chat )
                {
                    teleglax::InputFile infile{teleglax::InputFile::Type::Custom, ""};

                    if ( match[3] == "https://" || match[3] == "http://" )
                        infile = teleglax::InputFile(teleglax::InputFile::Type::Url, match[2]);
                    else if ( match[3] == "file://" )
                        infile = teleglax::InputFile(teleglax::InputFile::Type::Path, match[4]);
                    else
                        infile = teleglax::InputFile(teleglax::InputFile::Type::Path, match[3]);

                    teleglax::MessageContent content;
                    if ( match[1] == "/sticker" )
                        content = teleglax::msg::Sticker(infile);
                    else if ( match[1] == "/photo" )
                        content = teleglax::msg::Photo(infile);
                    else
                        content = teleglax::msg::Document(infile);

                    teleglax::Message sent = current_chat.send_message(std::move(content)).get();
                    std::cout << "file: " << sent.content_media_file().id() << '\n';
                }
        });
        commands.add_command("/loglevel\\s+([0-9]+)",
            [](const std::smatch& match){
                td::Log::set_verbosity_level(std::stoi(match[1]));
        });
        commands.add_command("/file\\s+(-?[0-9]+)",
            [this](const std::smatch& match){
                std::int32_t id = std::stol(match[1]);
                teleglax::File file(teleglax, id);

                std::cout << "Size: " << file.size() << "\n";
                std::cout << "Path: " << file.path() << "\n";
                std::cout << "R.ID: " << file.remote_id() << "\n";

                std::cout << "Down: ";
                if ( file.is_downloading_completed() )
                    std::cout << "100%\n";
                else if ( file.can_be_downloaded() )
                    std::cout << "(disabled)\n";
                else
                    std::cout << file.downloaded_size() * 100 / file.size() << "%\n";

                std::cout << "Up  : ";
                if ( file.is_uploading_completed() )
                    std::cout << "100%\n";
                else
                    std::cout << file.uploaded_size() * 100 / file.size() << "%\n";
        });
        commands.add_command("(/me|/u(?:ser)\\s+([0-9]+))",
            [this](const std::smatch& match){
                teleglax::User user = match[1] == "/me" ? teleglax.me().get() : teleglax::User(teleglax, std::stol(match[2]));

                std::cout << "First name: " << user.first_name() << "\n";
                std::cout << "Last name : " << user.last_name() << "\n";
                std::cout << "Username  : " << user.username() << "\n";
                if ( !user.phone_number().empty() )
                    std::cout << "Phone     : " << user.phone_number() << "\n";
                std::cout << "Type      : ";
                switch ( user.type() )
                {
                    case teleglax::User::Type::Regular: std::cout << "Regular\n"; break;
                    case teleglax::User::Type::Deleted: std::cout << "Deleted\n"; break;
                    case teleglax::User::Type::Bot:     std::cout << "Bot\n";     break;
                    case teleglax::User::Type::Unknown: std::cout << "Unknown\n"; break;
                }
                std::cout << "ID        : " << user.id() << "\n";
        });
        commands.add_command("(?:/list_users|/listusers|/list_users|/lu)",
            [this](const std::smatch&){
                if ( current_chat )
                {
                    auto members = current_chat.chat_members().get();
                    if ( members.empty() )
                        std::cout << "(empty)\n";
                    for ( const auto& cm : members )
                    {
                        std::cout << "* " << cm.user().full_name();
                        if ( cm.status() == teleglax::ChatMember::Status::Creator )
                            std::cout << " [creator]";
                        if ( cm.status() == teleglax::ChatMember::Status::Administrator )
                            std::cout << " [admin]";
                        if ( cm.is_installed_bot() )
                            std::cout << " [bot]";
                        std::cout << "\n";
                    }
                }
        });
        commands.add_command("(?:/chat_info|/ci) ([-]?[0-9]+)",
            [this](const std::smatch& match){
                teleglax::Chat chat(teleglax, std::stoll(match[1]));
                std::cout << "Title: " << chat.title() << '\n';
                std::cout << "Username: " << chat.username() << '\n';
                std::cout << "Link: " << chat.invite_link() << '\n';
        });
        commands.add_command("(?:/most_shared)\\s*([0-9]*)",
            [this](const std::smatch& match)
            {
                std::unordered_map<teleglax::User::id_type, int> counters;

                for ( const teleglax::Chat& chat : teleglax.get_chats_range())
                {
                    switch ( chat.type() )
                    {
                        case teleglax::Chat::Type::BasicGroup:
                        case teleglax::Chat::Type::Supergroup:
                            try {
                                std::cerr << "\r\x1b[2KProcessing " << chat.id()
                                    << ' ' << chat.title() << "..."; std::cerr.flush();
                                for ( const teleglax::ChatMember& mem : chat.chat_members().get() )
                                {
                                    counters[mem.user().id()]++;
                                }
                            } catch ( const teleglax::ApiError & e ) {
                                std::cerr << " Skipped: "
                                    << e.error_code() << ' ' << e.what() << '\n';
                            }
                            break;
                        default:
                            break;
                    }
                }

                std::cerr << "\r\x1b[2K";

                std::size_t max_n = match[1].length() == 0 ? 1 : std::stoul(match[1]);

                using pair_type = std::pair<teleglax::User::id_type, int>;
                auto cmp = [](const pair_type& a, const pair_type& b){
                    return a.second > b.second;
                };
                std::priority_queue<pair_type, std::vector<pair_type>, decltype(cmp)> pq(cmp);
                auto my_id = teleglax.me().get().id();

                for ( const auto& p : counters )
                {
                    if ( p.first != my_id )
                    {
                        pq.push(p);
                        if ( pq.size() > max_n )
                            pq.pop();
                    }
                }

                if ( pq.empty() )
                {
                    std::cout << "No one found :(\n";
                }
                while ( !pq.empty() )
                {
                    auto p = pq.top();
                    pq.pop();
                    teleglax::User u{teleglax, p.first};
                    std::cout << "User:     " << u.full_name() << '\n';
                    std::cout << "Username: " << u.username() << '\n';
                    std::cout << "Id:       " << u.id() << '\n';
                    std::cout << "Chats:    " << p.second << '\n';
                    std::cout << '\n';
                }
            }
        );
        commands.add_command("(?:/common_groups|/cg)\\s+([0-9]+)",
            [this](const std::smatch& match){
                teleglax::User source(teleglax, std::stol(match[1]));

                for ( const auto& chat : source.groups_in_common().get() )
                    std::cout << " * " << chat.id() << " " << chat.title() << '\n';
        });
        commands.add_command("(?:/advanced_search|/s\\+)",
            [this](const std::smatch&){
                if ( !current_chat )
                    return;

                std::string query;
                std::cout << "Query: ";
                std::cout.flush();
                std::getline(std::cin, query);

                std::string input;
                teleglax::User::id_type uid = 0;
                std::cout << "User id [0]: ";
                std::cout.flush();
                std::getline(std::cin, input);
                try { uid = std::stoll(input); } catch ( const std::exception& ) {}

                teleglax::Chat::SearchFilter sf = teleglax::Chat::SearchFilter::Empty;
                std::cout << "Message type [0]:\n"
                        << "\t0 - Any\n"
                        << "\t1 - Picture\n"
                        << "\t2 - Gif\n"
                        << "\t3 - Link\n"
                        << "\t4 - File\n";
                int index = 0;
                std::getline(std::cin, input);
                try { index = std::stoi(input); } catch ( const std::exception& ) {}
                switch ( index )
                {
                    case 1: sf = teleglax::Chat::SearchFilter::Photo;       break;
                    case 2: sf = teleglax::Chat::SearchFilter::Animation;   break;
                    case 3: sf = teleglax::Chat::SearchFilter::Url;         break;
                    case 4: sf = teleglax::Chat::SearchFilter::Document;    break;
                }

                bool shown = false;
                for ( const teleglax::Message& msg : current_chat.search_range(query, 0, uid, sf) )
                {
                    if ( !shown )
                    {
                        shown = true;
                        std::cout << "Enter for older messages, q to stop searching\n";
                    }
                    print_message(msg);
                    std::getline(std::cin, input);
                    if ( input == "q" )
                        break;
                }
                if ( shown )
                    std::cout << "No more results\n";
                else
                    std::cout << "No result\n";
        });

    }

    bool loop()
    {
        if ( !teleglax.connect() )
        {
            std::cerr << "Connection failed\n";
            return false;
        }
        std::cout << "Connected\n";
        thread = std::thread([this]{run_tread();});

        std::string cmd;
        try
        {
            while ( run )
            {
                std::getline(std::cin, cmd);
                if ( std::cin.eof() )
                    break;

                try {
                    commands.run(cmd);
                } catch ( const teleglax::UnknownObjectError& e ) {
                    std::cerr << e.what() << '\n';
                } catch ( const teleglax::ApiError& e ) {
                    std::cerr << "Error " << e.error_code() << ": " << e.what() << '\n';
                }
            }
        }
        catch ( const std::exception& e )
        {
            std::cerr << "Error: " << e.what() << '\n';
            thread.join();
            return false;
        }
        run = false;

        thread.join();
        std::cerr << "Exiting\n";
        return true;
    }

private:
    void run_tread()
    {
        while ( run )
        {
            try {
                teleglax.poll(1);
            } catch ( const teleglax::UnknownObjectError& e ) {
                std::cerr << e.what() << '\n';
            } catch ( const teleglax::ApiError& e ) {
                std::cerr << "Error " << e.error_code() << ": " << e.what() << '\n';
            } catch ( const std::exception& e ) {
                std::cerr << "Error: " << e.what() << '\n';
                run = false;
            }
        }
    }

    void print_message(const teleglax::Message& message)
    {
        std::string text;
        switch ( message.content_type() )
        {
            case teleglax::Message::ContentType::Text:
                text = message.content_text();
                break;
            case teleglax::Message::ContentType::Animation:
                text = "[Animation] " + message.content_text();
                break;
            case teleglax::Message::ContentType::Audio:
                text = "[Audio] " + message.content_text();
                break;
            case teleglax::Message::ContentType::Document:
                text = "[Document] " + message.content_text();
                break;
            case teleglax::Message::ContentType::Photo:
                text = "[Photo] " + message.content_text();
                break;
            case teleglax::Message::ContentType::ExpiredPhoto:
                text = "[ExpiredPhoto]";
                break;
            case teleglax::Message::ContentType::Sticker:
                text = "[Sticker] " + message.content_text();
                break;
            case teleglax::Message::ContentType::Video:
                text = "[Video] " + message.content_text();
                break;
            case teleglax::Message::ContentType::ExpiredVideo:
                text = "[ExpiredVideo]";
                break;
            case teleglax::Message::ContentType::VideoNote:
                text = "[Video]";
                break;
            case teleglax::Message::ContentType::VoiceNote:
                text = "[VoiceNote] " + message.content_text();
                break;
            case teleglax::Message::ContentType::Location:
            {
                auto location = message.content_as<td::td_api::messageLocation>();
                text = "[Location] "
                    + std::to_string(location->location_->longitude_)
                    + "," + std::to_string(location->location_->latitude_);
                break;
            }
            case teleglax::Message::ContentType::Venue:
                text = "[Venue] " + message.content_text();
                break;
            case teleglax::Message::ContentType::Contact:
            {
                auto contact = message.content_as<td::td_api::messageContact>();
                text = "[Contact] "
                    + contact->contact_->first_name_
                    + " " + contact->contact_->last_name_;
                break;
            }
            case teleglax::Message::ContentType::Game:
            {
                auto msg = message.content_as<td::td_api::messageGame>();
                text = "[Game] " + msg->game_->title_ + " " + msg->game_->text_->text_;
                break;
            }
            case teleglax::Message::ContentType::Invoice:
                text = "[Invoice]";
                break;
            case teleglax::Message::ContentType::Call:
                text = "[Call]";
                break;
            case teleglax::Message::ContentType::BasicGroupChatCreate:
                text = "* created group " + message.content_text() + " *";
                break;
            case teleglax::Message::ContentType::SupergroupChatCreate:
                text = "* created super group  " + message.content_text() + " *";
                break;
            case teleglax::Message::ContentType::ChatChangeTitle:
                text = "* renamed chat " + message.content_text() + " *";
                break;
            case teleglax::Message::ContentType::ChatChangePhoto:
                text = "* changed chat picture *";
                break;
            case teleglax::Message::ContentType::ChatDeletePhoto:
                text = "* deleted chat picture *";
                break;
            case teleglax::Message::ContentType::ChatAddMembers:
            {
                auto msg = message.content_as<td::td_api::messageChatAddMembers>();
                text = "* addded ";
                bool comma = false;
                for ( const auto& id : msg->member_user_ids_ )
                {
                    if ( comma )
                        text += ", ";
                    text += teleglax::User(teleglax, id).full_name();
                    comma = true;
                }
                text += " *";
                break;
            }
            case teleglax::Message::ContentType::ChatJoinByLink:
                text = "* joined *";
                break;
            case teleglax::Message::ContentType::ChatDeleteMember:
            {
                auto msg = message.content_as<td::td_api::messageChatDeleteMember>();
                text = "* removed user " + teleglax::User(teleglax, msg->user_id_).full_name() + " *";
                break;
            }
            case teleglax::Message::ContentType::ChatUpgradeTo:
                text = "* upgraded to supergroup *";
                break;
            case teleglax::Message::ContentType::ChatUpgradeFrom:
                text = "* upgraded to supergroup *";
                break;
            case teleglax::Message::ContentType::PinMessage:
                text = "* pinned a message *";
                break;
            case teleglax::Message::ContentType::ScreenshotTaken:
                text = "* took a screenshot *";
                break;
            case teleglax::Message::ContentType::ChatSetTtl:
            case teleglax::Message::ContentType::CustomServiceAction:
            case teleglax::Message::ContentType::GameScore:
            case teleglax::Message::ContentType::PaymentSuccessful:
            case teleglax::Message::ContentType::PaymentSuccessfulBot:
            case teleglax::Message::ContentType::ContactRegistered:
            case teleglax::Message::ContentType::WebsiteConnected:
            case teleglax::Message::ContentType::Unsupported:
            default:
                text = "[Unsupported Message]";
                break;
        };

        std::cout << "<" << message.sender().full_name() << "> " << text << '\n';
    }

    teleglax::Teleglax teleglax;
    std::atomic<bool> run{true};
    std::thread thread;
    CommandRunner commands;
    teleglax::Chat current_chat{&teleglax, 0};
};

int main(int argc, char **argv)
{
    if ( argc < 3 )
    {
        std::cerr << "Usage:\n" << argv[0] << " id hash\n\n";
        return 1;
    }

    teleglax::AppInfo app_info = {
        std::stoi(argv[1]),
        argv[2],
        "1.0",
    };
    teleglax::fs::path base_path = teleglax::home() / ".config" / "teleglax";
    app_info.database_directory = base_path / "db" / "teleglax-cli";
    app_info.files_directory = base_path / "files";
    app_info.use_message_database = true;
    app_info.enable_storage_optimizer = true;
    app_info.use_secret_chats = true;

    td::Log::set_verbosity_level(1);

    Shell shell(app_info);
    if ( !shell.loop() )
        return 1;
    return 0;
}
